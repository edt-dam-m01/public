### Exercicis FTP

L'objectiu d'aquest exercici és muntar un servidor ftp que permeti usuaris
anònims. Entre d'altres funcionalitats, voldrem que els usuaris anònims a més
de baixar arxius també puguin pujar fitxers al servidor però no els puguin
esborrar una vegada pujats. Per aconseguir-ho aneu responent els següents
apartats.

##### Exercici 1

* Que volen dir les sigles FTP?
* Defineix FTP.
* A quina capa del model TCP/IP es troba?

##### Exercici 2

Llegint aquest interessant enllaç [FTP actiu vs
passiu](http://slacksite.com/other/ftp.html) responeu a les següents preguntes:

* Quin tipus d'arquitectura clàssica utilitza aquest protocol?

* Quants canals de comunicació s'utilitzen? Quins?

* Quin són els ports habituals del servidor? Digues per a quin canal són cadascú.

* Descriu breument com funciona FTP actiu.

* Descriu breument com funciona FTP passiu.

##### Exercici 3

Si no tens instal·lat el paquet `ftp`, instal·la'l.

Comprova si és un client `ftp`: podem fer una *query* a la base de
dades del sistema que conté els paquets instal·lats amb l'ordre `rpm` (veure al
man les opcions de *query* i *all*) i filtrar els resultats amb l'ordre de bash
habitual.

Per obtenir informació detallada d'un paquet instal·lat pots fer servir la
mateixa ordre fent una query d'informació passant-li com a argument el nom del
paquet.

##### Exercici 4

Quin és el paquet recomanable per muntar un servidor amb Fedora? Instal·la'l.

##### Exercici 5

Ja em puc connectar o necessito fer alguna cosa més? Prova-ho connectant-te de
manera local, és a dir fent:

```
ftp 127.0.0.1 
```
##### Exercici 6

Si volem entrar de manera anònima, quin "name" (login) he de fer servir al
connectar-me? I contrasenya? Un cop he entrat, per *ftp*, a quin directori em
trobo? Ho puc veure a algun fitxer sense necessitat de connectar-me? 

##### Exercici 7

Proveu de crear al servidor un directori de nom `downloads` i dintre un fitxer
anomenat `virus.exe`. Posteriorment descarrega des d'un client el fitxer
`virus.exe` com a usuari anònim.

##### Exercici 8
  
Proveu de crear al servidor un directori de nom uploads. Posteriorment pugeu
des d'un client un fitxer qualsevol com a usuari anònim. Haureu de 
configurar alguna cosa abans.

##### Exercici 9

Al directori `downloads` podem també pujar fitxers? Si és que sí com podríem
solucionar-ho perquè a aquest directori no poguéssim pujar res, però a
`uploads` sí?

##### Exercici 10

Si feu un cop d'ull a l'[enllaç dels desenvolupadors de
ftp](https://www.gnu.org/software/inetutils/manual/inetutils.html#ftp-invocation)
veureu que hi ha una invocació diferent `pftp` que en realitat crida a ftp en
mode passiu:

```
[mpunto@h155 ~]$ ls -l /usr/bin/pftp
lrwxrwxrwx. 1 root root 3 Feb  3  2016 /usr/bin/pftp -> ftp
```

Abans ftp es connectava en mode actiu per defecte i si es volia connectar en
mode passiu, havíem d'indicar-ho `ftp -p` o `pftp`, però ara les coses han
canviat. Per què?
