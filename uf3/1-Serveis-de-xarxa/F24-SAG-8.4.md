### Fedora 24 - System Administrator Guide. Chapter 8.4.OpenSSH clients (ssh, scp, sftp)

Fedora24- System Administrator Guide - chapter 8.4 OpenSSH clients:

* [ssh](https://docs.fedoraproject.org/en-US/Fedora/24/html/System_Administrators_Guide/s1-ssh-clients.html)
* [scp](https://docs.fedoraproject.org/en-US/Fedora/24/html/System_Administrators_Guide/s2-ssh-clients-scp.html)
* [sftp](https://docs.fedoraproject.org/en-US/Fedora/24/html/System_Administrators_Guide/s2-ssh-clients-sftp.html)
