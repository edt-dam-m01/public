### Enllaços teoria NFS

Alguns enllaços útils per resoldre la pràctica NFS:

* [Configure NFS (general)](http://lpic2.unix.nl/ch10s02.html)
* [Configure NFS server](https://www.server-world.info/en/note?os=Fedora_24&p=nfs)
* [Configure NFS client](https://www.server-world.info/en/note?os=Fedora_24&p=nfs&f=2)

![arquitectura protocol NFS](NFS_Teoria.png)

