### Exercicis introducció a SSH

_Treballarem ara amb informació encriptada mitjançant SSH. Primer de tot hem de
comprovar que tenim instal·lats els paquets adients. Aquest servei és tan útil
per a a les comunicacions que normalment els paquets necessaris ja es troben
instal·lats._

##### Exercici 1

Que vol dir *SSH*? Defineix *SSH*. A quina capa del model *TCP/IP* es troba
aquest protocol?

##### Exercici 2

Quin és el paquet server per a Fedora 24? I el paquet client per a Fedora 24?
Per saber si un cert paquet està instal·lat al nostre sistema:

```
rpm -aq | grep el_paquet_que_busquem
```

Per trobar tots els paquets disponibles (instal·lats i no instal·lats) que es
diuen com el paquet que busquem:

```
dnf search paquet
```

I amb l'ordre:

```
rpm -qi paquet
```

es troba informació extra del paquet «paquet»

##### Exercici 3

Com inicialitzem el servei ssh al servidor ? i perquè arrenqui cada cop que
s'iniciï el sistema?

##### Exercici 4 

On es troba el fitxer de configuració del servidor? I del client? Quina
directiva (i de quin fitxer?) permet [o no] accedir a l'usuari root remotament?
I quina directiva (i de quin fitxer?) permet [o no] filtrar
usuaris per accedir remotament?

##### Exercici 5

Què contè el fitxer `~/.ssh/known_hosts`? (podem tornar a fer un cop d'ull a
aquest fitxer després de fer l'exercici següent)

##### Exercici 6

Com fem una connexió des del client al servidor, mantenint una sessió interactiva? 

##### Exercici 7

I si l’usuari per accedir al servidor és el mateix que l’usuari que utilitzem
al client, com escriuries l’ordre anterior?

##### Exercici 8

Com executem una única comanda des del client al servidor? Per exemple si volem
llistar el directori tmp del servidor `ls -l /tmp`

##### Exercici 9

I si el SSH server està escoltant al port 2222, en lloc del port 22 que és el
port per defecte, com seria l’ordre per obrir una sessió `ssh`?

##### Exercici 10 

Ara sense connectar-nos remotament volem copiar un fitxer de la màquina remota,
	el nom del qual coneixem, que es troba a `/tmp`. Quina seria l'ordre?
	Comproveu-lo. I si fos un directori?

##### Exercici 11

I si volem copiar com abans (de forma remota) però el port per on escolta SSH
també ha canviat, per exemple el 2222?

##### Exercici 12

Si iniciem una sessió amb `ssh` i intentem executar al servidor una eina
gràfica com podria ser `gnome-calculator` o `firefox`. Algun problema?

##### Exercici 13

El client de `ssh` té la possibilitat de reenviar aplicacions X11 desde el
servidor gràfic. Aquesta técnica, anomenada reenviament per X11, en anglès X11
Forwarding proporciona un medi segur per utilitzar aplicacions gràfiques sobre
una xarxa. 

Alerta amb l'arquitectura client servidor que s'aplica tant al protocol *ssh*
com al *X Window*, però a on els 2 ordinadors intercanvien els papers:

> “In X, the server runs on the user's computer, while the clients may run on
> different machines. This reverses the common configuration of client–server
> systems, where the client runs on the user's computer and the server runs on
> a remote computer. This reversal often confuses new X users. The X Window
> terminology takes the perspective of the program, rather than of the end-user
> or of the hardware: the remote programs connect to the X server display
> running on the local machine, and thus act as clients; the local X display
> accepts incoming traffic, and thus acts as a server.” (WIKIPEDIA)

Hi ha fins a 3 maneres de resoldre el problema anterior, es necessita
configurar d'alguna manera el servidor SSH? i el client SSH?

Amb l'ajuda del `man sshd_config` busca la directiva d'aquest fitxer que hem de 
posar al servidor per tal que el servidor ho permeti. (Hint: X11)

Amb l'ajuda de `man ssh_config` descriu les 3 possibles
maneres de poder executar aplicacions gràfiques per ssh.

##### Exercici 14

Com faries la connexió ssh des de el navegador d'arxius (Files, nautilus)?

##### Exercici 15

Per si de cas, feu una còpia del fitxer original `/etc/ssh/sshd_config`
(`/etc/ssh/sshd_config.bak`) i feu els següents canvis a
`/etc/ssh/sshd_config`:

+ No permetre l'accès a *root*.
+ Permetre només l'accès a un usuari iawXXXX i a un usuari local.
+ Comproveu que tot funciona

#### Seguretat ftp versus ssh

##### Exercici 1

Recordeu que els ports 20 i 21 estan reservats per servidors *ftp* (tot i que
el 20 fos el del canal de dades en ftp actives i aquestes ja no s'utilitzen).
Quin és el port habitual de connexió `ssh`?

##### Exercici 2

Ara heu de comprovar que amb el protocol `ftp` les dades viatgen en fitxers
plans, sense encriptar i podrem veure la contrasenya de l'usuari local (user1,
guest ...) que cadascú faci servir. Heu d'utilitzar `wireshark`. I després heu
de fer el mateix amb una connexió `ssh`.

