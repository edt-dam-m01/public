### Exercicis NFS

##### Exercici 0: introducció NFS

Volem treballar amb NFS, però abans seria bo conèixer una mica de quin protocol
parlem:

* Que volen dir les sigles NFS ?
* Quina és la funció principal d'aquest protocol?
* Quina empresa, que va ser la que més va contribuir al codi obert de tot el
mon, va inventar aquest protocol?
* Quin llenguatge de programació va inventar aquesta empresa? Y quin sistema
gestor de bases de dades?
* Quina és l'arquitectura (molt habitual en el món de les xarxes) d'aquest
protocol?
* Segons el model OSI, a quina capa pertany el protocol NFS? I segons el model
TCP/IP?
* Fem servir nosaltres, o sigui el departament d'informàtica, aquest protocol?
Si la resposta és afirmativa, descriu-la una mica. (Hint: *mount*)

##### Exercici 1: instal·lació de NFS

Utilitzarem 2 pc's: un farà de servidor (exportarà directori/s) i l'altre de client (podrà veure i/o manipular fitxers). Per tant es tracta de fer la feina per parelles (d'usuaris i de pc's).

* Es necessita algun paquet específic, quin és?
* Quina és la comanda que "xequeja" si existeix aquest paquet?
* En el cas de no trobar-se al sistema, com s'instal·la?

##### Exercici 2: inici del servei NFS 

Ara volem iniciar el servei nfs (dimoniet).

Com iniciem ara mateix al servidor el dimoni NFS? I per a les properes vegades?

##### Exercici 3: exportació de directoris (Configuració al servidor)

En el nostre exemple treballarem amb directoris nous. Però el més habitual és
que els directoris que volem exportar ja existeixin (com per exemple el
		directori `/home`).

* Creeu els directoris `/share1` i `/share2` amb permisos per a fer de tot per a
tothom.
* Modifiquem el fitxer `/etc/exports` de manera que tant el directori `/share1`
com el `/share2` només el pugui veure el seu pc veí. El directori `/share1`
serà un directori d'escriptura i lectura mentre que el directori `/share2`
només ho serà de lectura.
* Quina és l'ordre que fa que el dimoniet `nfsd` es torni a llegir el fitxer
`/etc/exports` per si hi ha algun canvi?

##### Exercici 4: compartició de fitxers (configuració al client)

Al pc client hem de decidir quin seran els directoris sobre els quals muntarem
els directoris *exportats* del servidor (o sigui els que es volen compartir).

Quines són les ordres que hem d'executar al client per muntar els directoris
exportables `/share1` i `/share2` del servidor?

##### Exercici 5: muntatge automàtic

Ara volem que al pc client es munti automàticament el directori `/share1` del
servidor. Com ho faries?

I si volem que es puguin connectar tots els pc's *192.168.X.X*?

##### Links ajuda

Recordeu que pot ser d'ajuda [aquest enllaç](NFS_Teoria.md)

