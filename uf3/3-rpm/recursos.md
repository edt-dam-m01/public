### Recursos de gestors de paquets ( dnf, yum , apt-get ...)

A banda dels habituals del LPI i Sergi Tur, és recomanable:

* [La referència d'ordres proporcionada per l'equip del projecte *dnf*](http://dnf.readthedocs.io/en/latest/command_ref.html)
* [Una comparativa exhaustiva dels diferents gestors de paquets GNU/Linux](https://fusion809.github.io/comparison-of-package-managers)
* [Introducció als diferents gestors de paquets](https://www.digitalocean.com/community/tutorials/package-management-basics-apt-yum-dnf-pkg)
* [Migració de yum a dnf: `man yum2dnf`](https://www.mankier.com/8/yum2dnf)
