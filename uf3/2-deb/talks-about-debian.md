## Talks about Debian (Mònica Ramírez)

Algunes xerrades de Mònica Ramírez, desenvolupadora de Debian i professora de l'IES Escola del Treball.

Tot i que ja estan una mica deprecated, recomanables:

- [Gestió de paquets mab Debian (2007)](http://dunetna.probeta.net/doku.php/debian:talk_debian_gestio_paquets_debian)
- [El sistema operatiu lliure creat per la comunitat (2012)](http://dunetna.probeta.net/doku.php/debian:talk_debian_institut_ausias_march)

Feu clic a l'enllaç [http://dunetna.probeta.net/doku.php/debian:debian_talks](http://dunetna.probeta.net/doku.php/debian:debian_talks) per a obrir el recurs.
