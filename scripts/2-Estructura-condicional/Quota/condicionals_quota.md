# Script de quota

Fes l'script de quota. Utilitza el fitxer [quota_warning_comentaris.sh](quota_warning_comentaris.sh) que faràs servir d'esquelet per començar a treballar amb ell. Intenta fer els passos (traduir els comentaris a llenguatge bash scripting) a poc a poc.

##### OBSERVACIÓ

Recordeu que amb `$(( ))` també podem fer comparacions condicionals però només amb enters. Per exemple:

```
echo $((7/5 == 4/3)) 
```

dóna TRUE, ja que al fer la divisió entera, la pregunta s'interpreta com a `1 == 1 ?`

mentre que

```
echo "7/5 == 4/3" | bc -l 
```

dóna FALSE, ja que la pregunta s'interpreta com a `1.4  == 1.333 ?`
 
