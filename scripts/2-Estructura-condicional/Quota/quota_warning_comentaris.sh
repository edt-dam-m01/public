#!/bin/bash
# Filename:		quota_warning.sh
# Author:		pingui
# Date:			16/11/2017
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
# 				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./quota_warning.sh [percentatge_max]
# Description:	Script que calcula la mida dels fitxers emmagatzemats al nostre directori de gandhi
# 				i mostra missatge d'error si supera un cert límit


# Si hi ha arguments s'agafarà el primer argument com a percentatge màxim
# altrament, el percentatge límit serà una constant fixa

# Emmagatzemem a una variable la quota de l'usuari:

# Emmagatzemem a una variable l'espai utilitzat

# Calculem el percentatge (% 100) utilitzat per l'usuari

# Mirem si el percentage utilitzat és més gran que el límit d'avís que ens marca l'script
# A la següent comparació relacional obtindrem un 1 si és cert (0 si és fals)

# Si el resultat és 1 mostrem un missatge d'alerta
