Teniu al moodle a dintre de "Exemples_intro_condicional" els scripts que vam fer a classe de les sumes i de la divisió utilitzant l'ordre bc.

Hauríeu de fer els següents scripts (sempre amb capçalera i comentaris):

1. Un script com divisio.sh que demani 2 arguments, dividend (`D`) i divisor (`d`), i doni, com a resposta, la prova de la divisió.
   Si la divisió és exacta, escriurà `D=q*d` (on `q` representa el quocient) i si la divisió no és exacta, escriurà `D=q*d+r` (on `r`
   és el residu).

2. Jugueu amb `bc` amb expressions relacionals, per exemple:
    * 2 < 4
    * 2 >= 4
    * 3 != 3
    * 3 

    Esbrineu quin és el resultat de bc en funció que l'expressió sigui certa o no. Un cop fet això,
    feu un script que rebi una expressió condicional i que acabi mostrant `TRUE` o `FALSE` en funció
    que sigui certa o falsa l'expressió condicional. Haureu de resoldre un petit problema que podeu
    trobar amb el redireccionament.

3. Feu un fitxer `dades.dat` amb dues columnes de números. Per exemple:

>5 7

>8 2

>0 4

>5 -3

   Després feu un script `opera.sh` que llegeixi el contingut d'aquest fitxer, li'l passi a `bc` i mostri diferents operacions, d'una forma amigable. Una cosa com:

>5 + 7 = 12

>8 + 2 = 10

>...

>5 / 7 = 0,714

>etc


OBS: Amb `$(( ))` també podem fer comparacions condicionals però només amb enters, per exemple
`echo $((7/5 == 4/3))` dóna `TRUE`, ja que en fer la divisió entera, ho interpreta com a `1 == 1`, 
mentre que `echo "7/5 == 4/3" | bc -l`  dóna `FALSE`   (en efecte, `1.4  != 1.333` ).
