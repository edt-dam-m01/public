### SHELL­SCRIPTS

Feu els exercicis següents extrets del 
[manual de shell scripts](http://dunetna.probeta.net/lib/exe/fetch.php/documentation:cursshellscript-catala.pdf), un document molt interessant per a fer consultes i pràctiques.

Utrilitzeu aquest [document d'ajuda amb els comentaris](ExercicisCondicionalsShellScripts-Ajuda.md).

29. Dissenyar un script que donats dos arguments els sumi si el primer és menor que el segon
i els resti en cas contrari.

30. Dissenyar un script que demani un caràcter i ens digui si és un número, una lletra o una altra cosa.

31. Dissenyar un script al qual se li passa un argument i si aquest és un directori llista el seu contingut.

32. Dissenyar un script que suma tots els números que se li passen per paràmetre. _Hint:Ordres_ `cut` o `tr`.
