### Millora script header

Volem fer ara una nova versió del nostre header amb les següents noves
funcionalitats (*features*):

* L'script comprovarà si el nom de l'script acaba en _.sh_, en cas negatiu afegirà la cadena _.sh_ al nom que hagi escollit l'usuari.

* L'script comprovarà si el fitxer en qüestió ja existeix. En cas afirmatiu, sortirà de l'script informant de l'error a l'usuari i també al sistema.


