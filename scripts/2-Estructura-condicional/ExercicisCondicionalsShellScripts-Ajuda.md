## Exercici 29 Ajuda

```
#!/bin/bash
# Filename:	exercici29.sh
# Author:	pingui
# Date:		05/02/2018
# Version:	0.1
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./exercici29.sh num1 num2
# Description:	Donats dos arguments els suma si el 1er argument és menor que el segon
# 		i els resta en cas contrari.

# Si no tinc dos arguments mostro missatge d'error, ajuda
# i surto amb un valor que indiqui que hi ha hagut un error

# En cas contrari, si el primer argument és menor que el segon els sumo

# En cas constrari els resto

# Mostro el resultat
```

----------------------------------------------------

## Exercici 30 Ajuda

```
#!/bin/bash
# Filename:	exercici30.sh
# Author:	pingui
# Date:		05/02/2018
# Version:	0.2
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./exercici30.sh
# Description:	Entrarem un caràcter i direm si és un número, lletra o diferent d'aquests. 
# 		Tot i que l'exercici no diu res al respecte, farem un petit control d'errors: 
# 		si hi ha més o menys d'un caràcter, sortirem.

# Demanem un caràcter i l'emmagatzemem

# Si no hem entrat exactament un caracter sortim mostrant l'error a l'usuari i al sistema 

# En cas contrari, si és una lletra mostrem un missatge indicant-lo

# Altrament, si és un número mostrem un missatge indicant-lo

# Si no, mostrem un missatge indicant que no és ni lletra ni nombre
```

------------------------------------------------------------

## Exercici 31 Ajuda

```
#!/bin/bash
# Filename:	exercici31.sh
# Author:	pingui
# Date:		05/02/2018
# Version:	0.1
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./exercici31.sh file1
# Description:	Script que rep un argument. Si és un directori llista el seu contingut

# Si no es rep un únic argument es mostra un missatge d'error, d'ajuda i sortim indicant l'error al sistema.

# Altrament, si no existeix el fitxer mostro un missatge que indica que no existeix. Surto indicant l'error al sistema.

# Altrament, si no és un directori mostro un missatge que indica que no ho és.

# Altrament, fem un llistat de format llarg del directori
```

------------------------------------------------------------

## Exercici 32 Ajuda

```
#!/bin/bash
# Filename:	exercici32.sh
# Author:	pingui
# Date:		05/02/2018
# Version:	0.1
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./exercici31.sh file1
# Description:	Script que ssuma totsels números que se li passen per paràmetre

# Si no es rep menys de 2 arguments es mostra un missatge d'error, d'ajuda i sortim indicant l'error al sistema.

# Altrament, amb l'ordre cut modifica la llista d'arguments per tal d'afegir un signe "+" entre cada dos i ho passa a bc
```


