# Exercicis introducció a l'estructura condicional IF

Abans de fer els següents exercicis estudieu l'ordre read

El seu us més habitual és:
```
read nom_de_variable
```

* Mireu com funciona executant alguns exemples.
* Com recupero el contingut de la línia?
* En el cas que no poseu una variable, la línia que llegeix *read* s'emmagatzema en algun lloc o es perd? (hauríeu de cercar ajuda)
* Quin *errorlevel* em retorna l'execució de *read* si poso una línia “normal”?
* Quin *errorlevel* em retorna *read* si no escric res i directament polso l'intro?
* Recordeu que vam explicar que hi ha un caràcter que representa el final de fitxer? Feu la prova introduint aquest caràcter (sense l'intro). Quin *errorlevel* obteniu?

### Exercici 1

Feu un script *llegeix_linia.sh* que llegeixi una línia introduida per l'usuari i que mostri per pantalla el missatge

“*linia llegida: bla bla bla ...*”

i, en canvi si s'ha introduit el senyal de final d'arxiu mostrarà:

“*no s'ha introduit cap línia*”

### Exercici 2

Feu que l'script sigui més amable: Volem que es mostri un missatge de manera que l'usuari sàpiga què ha de fer quan executa l'script. Per exemple:

```
[vfornes@pc666 hell]$ ./llegeix_linia.sh
escriu el que vulguis i tot seguit prem intro
```

(Vigila dintre de l'script quina ordre escrius primer)

### Exercici 3

Ara volem diferenciar entre els 3 casos possibles que hem comentat abans:

- línia buida
- línia normal
- senyal de final de fitxer

Per tant l'script em mostrarà:

*linia llegida: buida*

*linia llegida: bla bla bla*

*no s'ha introduit cap línia*

respectivament

Hint 1: man test per saber si una cadena és buida.

Hint 2: es pot posar un if dintre d'un if?

### Nota

Perquè el nostre fitxer vim identi de manera automàtica, hem de posar

*set autoindent*

recordem a més que si volem que el tabulador sigui de 4 espais en blanc hauríem de posar

*set tabstop=4*

tamble podeu posar números

*set nu* (numbers)

etcètera

Si volem que això serveixi per sempre ho podem posar al fitxer *.vimrc* que ha d'estar al nostre HOME

**Recomanació molt important: mai, mai no utilitzeu una ordre per primera vegada dintre d'un script, abans jugueu amb ella des de la consola fins que enteneu bé com funciona.**
