#!/bin/bash
# Filename:	divisio.sh
# Author:	ordinari
# Date:		29/01/2018
# Version:	0.1
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./divisio.sh arg1 arg2 arg3
# Description:	Script que rep per la línia d'ordres 3 arguments
# 		que seran 3 nombres enters. Els divideix i mostra el resultat amb 
#		el nombre de decimals determinat pel 3r argument.
# 		Si no rep 3 arguments mostra un missatge d'error, una ajuda
# 		i surt de l'script amb un errorlevel diferent de 0

# Si el número d'arguments és diferent de 3 mostra missatges i surt de l'script
if [ $# -ne 3 ]
then
	echo "Número d'arguments incorrecte"
	echo -e "Ús:\t $0 integer1 integer2 integer3"
	exit 1  # Surt de l'script amb errorlevel 1
else # En cas contrari fa la divisió i la mostra amb el nombre de decimals demanat
	divisio=$(echo "scale=$3;$1 / $2" | bc -l)
	echo "El resultat de $1 entre $2 amb $3 decimals és $divisio"
fi

# (*) 	echo "scale=$3;$1 / $2" | bc -l  
# echo envia l'expressió "scale=$3;$1 / $2" a l'ordre bc -l, gràcies a la '|',
# un cop bc rep l'expressió la processa i mostra el resultat
# després $(    ) és substituït pel resultat de la divisió
# i finalment aquest resultat s'asigna a la variable divisio
