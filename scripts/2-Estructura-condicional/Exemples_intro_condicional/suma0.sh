#!/bin/bash
# Filename:	suma0.sh
# Author:	ordinari
# Date:		12/01/2018
# Version:	0.1
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./suma0.sh integer1 integer2
# Description:	Script que rep per la línia d'ordres 2 arguments que seran 2 nombres enters.
#		Els suma, i mostra el resultat.

# Mostrem la suma dels 2 números entrats per la línia d'ordres
echo $(($1+$2))

