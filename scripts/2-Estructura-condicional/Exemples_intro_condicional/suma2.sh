#!/bin/bash
# Filename:	suma2.sh
# Author:	ordinari
# Date:		12/01/2018
# Version:	0.3
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./suma2.sh integer1 integer2
# Description:	Script que rep per la línia d'ordres 2 arguments
# 		que seran 2 nombres enters. Els suma, i mostra el resultat.
# 		Si no rep 2 arguments mostra un missatge d'error, una ajuda
# 		i surt de l'script amb un errorlevel diferent de 0.



if [ $# -ne 2 ]
then # Si el número d'arguments és diferent de 2 mostra missatge i surt de l'script
	echo "Número d'arguments incorrecte."
	echo -e "Ús:\t $0 integer1 integer2"
	exit 1  # surt de l'script amb errorlevel 1
else # En cas contrari els suma i mostra el resultat
	suma=$(($1+$2))
	echo $suma
fi

# Si no posa res surt amb errorlevel igual a 0
# o sigui informa que l'script s'ha executat amb èxit

