#!/bin/bash
# Filename:	suma1.sh
# Author:	ordinari
# Date:		12/01/2018
# Version:	0.2
# License:	This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./suma1.sh integer1 integer2
# Description:	Script que rep per la línia d'ordres 2 arguments
# 		que seran 2 nombres enters. Els suma, i mostra el resultat.

# Sumem els 2 números i els emmagatzemem a la variable suma
suma=$(($1+$2))

# Mostrem el contingut de la variable suma
echo $suma
