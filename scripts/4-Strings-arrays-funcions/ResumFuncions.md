## Funcions

Suposem que tenim un script molt senzill que, donat un fitxer passat com a primer argument ens diu (escriu per pantalla) el tipus de fitxer què és, aprofitant el programa `file`.

Aquest script podria tindre de nom `tipus` i el seu contingut podria ser:

```
#!/bin/bash
file $1 | cut -d: -f2
```

Feu aquest script i proveu-lo amb fitxers de diferents tipus. Si voleu, podeu reduir una mica el seu resultat, tallant-lo fins a la primera coma: `file $1 | cut -d: -f2|cut -d, -f1`. I , ja que estem, eliminem l'espai inicial: `file $1:w
 | cut -d: -f2|cut -d, -f1|sed 's/^ //'`

Ara, suposem que volem fer un altre script, de nom `escriueltipus.sh` tal que, pregunti el nom d'un fitxer i, aprofitant l'script `tipus`, escrigui una cosa com: `El fitxer carta.txt és de tipus UTF-8 Unicode text`

Resumint molt (ja que no estem posant capçalera ni comentaris), aquest segon script podria tindre aquest contingut:

```
#!/bin/bash
read -p "Escriu el nom d'un fitxer: " fitxer
echo "El fitxer $fitxer és de tipus $(./tipus $fitxer)"
```

Doncs bé. Si aquest script auxiliar `tipus` només l'utilitzem en aquest altre script, podem utilitzar una funció, de manera que el contingut de l'script `escriueltipus.sh` podria ser (molt resumit)


```
#!/bin/bash
function tipus {
  file $1 | cut -d: -f2
}
read -p "Escriu el nom d'un fitxer: " fitxer
echo "El fitxer $fitxer és de tipus $(tipus $fitxer)
```

Simplificant, podríem dir que una funció és com un script, generalment senzill, que fa només una cosa i, normalment expressa el seu resultat escivint-lo per la sortida estàndard, però que, en compte d'estar en un fitxer, es manté en memòriai, per tant, s'esborra quan es tanqui la sessió del `bash` (o l'intèrpret d'ordres que estiguem utilitzant).

Igual com fan els scripts, també podem controlar el valor de l'error retornat per una funció. En els scripts usem `exti n` i en les funcions `return n`.

Fem una funció de nom `parell` que retorne `true` si el nombre que se li passa com a primer argument és parell i `false` en cas contrari:

```
function parell {
if [[ $1 =~ [02468]$ ]]
then
	return 0
else
	return 1
fi
}
```

Així podem contruir estructures com

```
if parell $nombre
then
...

```

O bé, comprovar el seu estat, després d'executar-la

```
parell $nombre
if [ $? -eq 0 ]
then
...
```
