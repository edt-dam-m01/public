### Resum de subcadenes

A partir d'aquests valors per a les variables:
```
abecedari="abcdefghijklmnopqrstuvwxyz"
index="abecedari"
```

amb aquestes ordres obtindrem aquests resultats

| Ordres | Resultats |
|--------|-----------|
| `echo ${abecedari:4}` | `efghijklmnopqrstuvwxyz` |
| `echo ${abecedari: -5}` | `vwxyz` |
| `echo ${abecedari:4:2}` | `ef` |
| `echo ${abecedari:0:5}` | `abcde` |
| `echo ${abecedari:6: -3}` | `ghijklmnopqrstuvw` |
| `echo ${abecedari: -6: -3}` | `uvw` |
| `echo ${abecedari: -6:2}` | `uv` |
| `echo ${#abecedari}` | `26` |
| `echo ${!index}` | `abcdefghijklmnopqrstuvwxyz` (expansió indirecta) |
| `echo ${index/e/--}` | `ab--cedari` (substitució una vegada) |
| `echo ${index//e/--}` | `ab--c--dari` (substitució totes les vegades) |

Recordeu que els nombres negatius han d'anar precedits d'un espai.

Per a més informació, `man bash`, seguit de `/  parameter expansion`

