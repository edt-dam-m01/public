### Pràctica amb funcions

Feu un script amb:
- Una funció que rebi un fitxer com a argument i escrigui per pantalla el seu propietari sense caràcter de nova línia.
- Una funció que rebi un fitxer com a argument i escrigui el grup
- Una funció que rebi un fitxer com a argument i escrigui la seva mida

En el cos de l'script, per cada fitxer del directori actual, comprovarà si és un fitxer regular i en cas afirmatiu, aprofitant les tres funcions anteriors, escrigui una frase com "El fitxer fit1 és propietat de l'usuari user1 i del grup group1 i té una mida de 465 B."
