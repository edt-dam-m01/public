#!/bin/bash
# Fitxer:	ex39_kp.sh
# Autor:	vfornes
# Data:		14/03/2018
# Versio: 0.1
# Descripcio:	Script que permet copiar (-c), moure (-m) o esborrar (-d) fitxers.
#		L'script ha de comprovar que la sintaxi utilitzada és la correcta.


# Observacions generals:
# Només permetrem que l'opció (-c, -m, -d) sigui el primer argument
# Es permet qualsevol nombre d'arguments sempre que sigui més gran que 2 per -d i més gran que 3 als altres casos.
# Farem una funcio per a cada una de les 3 accions.
# Tot i que no ho diu per motius de seguretat quan ens diguin d'esborrar directoris no ho farem, ni tan sols ho controlarem
# Tampoc no controlem que els fitxers existeixin


# funció esborra
# Si el nombre d'arguments és zero, ja hem descomptat l'argument -d, enviem un error al sistema
# Comprovem si tots els arguments passats són fitxers regulars
# En cas afirmatiu, esborrem tots els fitxers


# funció copia
# Si el nombre d'arguments és més petit que 2, ja haurem descomptat l'argument -c, enviem un error al sistema
# Altrament, si el nombre d'arguments és superior a 2 i el darrer argument no és un directori enviem un error al sistema.
# En cas contrari copiem tots els fitxers menys l'últim cap a l'últim, ja que si hi ha més de dos arguments, l'últim és un directori


# funció mou
# Si el nombre d'arguments és més petit que 2, ja haurem descomptat l'argument -m, enviem un error al sistema
# Altrament, si el nombre d'arguments és superior a 2 i el darrer argument no és un directori enviem un error al sistema.
# En cas contrari movem tots els fitxers menys l'últim cap a l'últim, ja que si hi ha més de dos arguments, l'últim és un directori
# Aqui podriem diferenciar en el cas de que hi hagin 2 arguments i el segon sigui un fitxer que existeix, però no ho farem


# Si el primer argument no és una de les 3 opcions, mostrem un missatge d'error i informem al sistema de l'error
# Altrament enviem els arguments a la funció corresponent perquè s'executi
# Si la funció ha fallat mostrem un missatge d'error i informem al sistema

