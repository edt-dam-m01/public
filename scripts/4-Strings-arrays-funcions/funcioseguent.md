Què fa aquesta funció?

```
function next {
   [ $1 -le 50 ] && echo $1 && next $(($1 + 1)) 
}
```

Penseu bé què fa i escriviu almenys dues funcions que facin el mateix utilitzant tècniques diferents.
