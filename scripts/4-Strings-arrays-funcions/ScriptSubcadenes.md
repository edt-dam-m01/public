### Script per a practicar subcadenes

Feu un script de nom `seguent.sh` que miri tots els fitxers que hi ha en el directori actual i 
per a cadascun d'ells, si és un fitxer regular, i el 4t caràcter del seu nom és un nombre (p. 
ex. abc4efg), creï un nou fitxer amb el nombre següent (abc5efg) amb el mateix contingut que el 
primer, afegint al final una línia nova amb la data actual.

Us caldrà, almenys, un `for` i treballar amb subcadenes `${A:n:n}`
