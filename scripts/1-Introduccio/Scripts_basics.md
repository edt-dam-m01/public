## Exercicis d'scripts bàsics

La dinàmica per a treballar cadascun d'aquests scripts serà:

1. Crea un directori on faràs tots aquests scripts.

2. Fes l'script, que faci **exactament** les coses que es demanen. No oblidis fer tota la capçalera. Això és important.

3. Fes-lo executable (chmod u+x nom_script.sh)

4. Executa'l amb ./nom_script.sh o l'ordre adequada i comprova que la sortida és exactament la que es busca

5. Almenys en els primers scripts, fes que el professor te'l revisi personalment.

Aquestes són les instruccions per a cadascun dels scripts que has de fer:

1. Fes un script de nom `simple.sh` que escrigui per pantalla:

   ```
   Hi!
   The current time and date are: Wed Nov 21 16:32:10 CET 2018
   ```

   (Ajuda: `echo -n` , `date`)

2. Fes un script de nom `mostra.sh` tal que si escrius `./mostra.sh Barcelona`, escrigui per pantalla:

   ```
   Barcelona
   ```

   I si escrius `./mostra.sh Vilanova i la Geltrú`, escrigui:

   ```
   Vilanova
   ```

És a dir, que mostri només el primer argument que li arribi per línia d'ordre. Prova també d'ajuntar diverses paraules entre cometes dobles, com a primer argument.

3. Fes un script de nom `mostra_tot.sh` que escrigui per pantalla tots els arguments que li arribin per línia d'ordres.

4. Fes un script de nom `guarda_en_fitxer.sh` que guardi tot els arguments que li arriben per la línia d'ordres en un fitxer de nom `missatge.txt`.

5.  Fes un script de nom `guarda_acumulat.sh` que guardi tot els arguments que li arriben per la línia d'ordres en un fitxer de nom `missatge_acumulat.txt`, però en aquest cas, no ha d'esborrar el fitxer, sinó que ha d'anar acumulant els missatges que li arriben, a la manera d'un log.

5. Fes un script de nom `crea_fitxer.sh` que creï un fitxer buit (amb l'ordre t`touch`) el nom del qual sigui el primer argument que li arribi per la línia d'ordres.

6. Fes un script de nom `missatge_fitxer.sh` que guardi el primer argument qu li arriba en un fitxer de nom el segon argument; per exemple, si escrivim `missatge_fitxer Hola text.txt` creï un fitxer de nom `text.txt` amb el contingut `Hola`





