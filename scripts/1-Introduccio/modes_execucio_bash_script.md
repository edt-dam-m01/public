## Modes d'execució de shell scripts

Anem a jugar una mica amb les diferents maneres que tenim d'executar un script.

### Que es llanci a una subshell de la shell activa

Això passarà quan el nostre fitxer no contingui el _shebang_, és a dir, la línia que indica amb quina _shell_ s'ha d'executar. Té l'inconvenient de no ser portable a _shells_ diferents de la qual es va crear.¹

Podem executar el nostre script de dues maneres:

1. `$ bash   nom_script`²
2. `$ ./nom_script`

Al primer cas no és necessari permisos d'execució, mentre que al segon cas sí.

__Estudiem la primera manera__

Fem un cas pràctic amb un script que simplement dorm 30 segons. Construïm-lo:
  ```
  echo "sleep 30" > dorm.sh
  ```
Si ara l'executem de la primera forma:
  ```
  bash dorm.sh
  ```
anem a veure (abans de que passin els 30 segons) l'arbre dels nostres processos. Des d'una altra terminal executem:
  ```
  pstree
  ```
  Es mostrarà:
  ```
  ├─terminator─┬─bash───bash───sleep
  │            ├─bash───pstree
  │            ├─{dconf worker}
  │            ├─{gdbus}
  │            ├─{gmain}
  │            └─{pool}
  ```
Veiem que terminator llença un procés fill _bash_ que és l'intèrpret de comandes (_shell_) des d'on nosaltres hem llençat un altre procés _bash_, el segon que surt a la pantalla, el qual al seu torn llença el procés fill sleep que és una instrucció que es troba dintre de l'script _dorm.sh_.

__Estudiem la segona manera__

Si donem permisos d'execució a l'script _dorm.sh_, l'executem:
  ```
  $ chmod u+x dorm.sh
  $ ./dorm.sh
  ```

Si fem el mateix d'abans veurem que `pstree` mostra un arbre semblant a l'anterior:

  ```
  ├─terminator─┬─bash───bash───sleep
  │            ├─bash───pstree
  │            ├─{dconf worker}
  │            ├─{gdbus}
  │            └─{gmain}
  ```
### Com a programa executable

Ara ho farem diferent. Afegirem **al principi del nostre script** la línia següent (o sigui el _shebang_):
  ```
  #!/bin/bash
  ```
que indica a la _shell_ amb quin programa s'executa el nostre script.

Executem-lo:
  ```
  ./dorm.sh
  ```
Si a un altra terminal fem `pstree` es mostra una cosa semblant a:
  ```
  ├─terminator─┬─bash───dorm.sh───sleep
  │            ├─bash───pstree
  │            ├─{dconf worker}
  │            ├─{gdbus}
  │            └─{gmain}
  ```
És a dir, _dorm.sh_ s'executa com a programa executable fill del procés _bash_, des d'on s'ha llençat l'script i al seu torn _sleep_ és un procés fill llençat des del procés dorm.sh


### A la mateixa shell activa


Per últim executem l'script d'una tercera forma, amb l'ordre `source` o l'equivalent `.`
  ```
  source dorm.sh
  ```
  o
  ```
  . ./dorm.sh
  ```
  veurem que no s'ha llençat ni un nou procés fill _bash_ com en el primer cas, ni un procés `dorm.sh` com en el segon, sinó que estem treballant al mateix nivell de _bash_ i són les diferents ordres del nostre script les que són processos fills del nostre _bash_.

  ```
  ├─terminator─┬─bash───sleep
  │            ├─bash───pstree
  │            ├─{dconf worker}
  │            ├─{gdbus}
  │            └─{gmain}
  ```

Si no es diu el contrari, nosaltres optarem sempre per la segona manera: definir el shebang i executar l'script com a programa executable.

Ara bé, l'elecció d'una manera o una altra a l'hora d'executar l'script té conseqüències molt interessants:
- Si creem una variable dintre d'un script, hi haurà visibilitat al sortir d'ell?
- Podré fer servir a l'script una variable que tingui definida al bash inicial?

__Exercici__: Jugueu una mica per resoldre aquests dubtes.


¹ És a dir si he fet un script per a shell bash, i l'script no informa d'això, és molt probable que en altre tipus de shell (korn-shell, c-shell ...) hi hagi moltes ordres que no funcionin.

² Bé en realitat, serà _bash_ si el _shell_ on s'executa l'script és el bash, si fos una altre, per exemple _tcsh_, llavors l'ordre seria:
  ```
  $ tcsh    nom_script
  ```
