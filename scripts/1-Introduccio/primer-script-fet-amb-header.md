## Primer script fet amb header

Aprofitant l'script `header.sh`, fem un script `compta.sh` que accepti un argument; per exemple

```
./compta.sh Fitxer4.txt
```

i respongui, en una línia una cosa com

```
El fitxer Fitxer4.txt té 654 caràcters, 246 paraules i 8 línies.
```
