El shebang és el conjunt de 2 caràcters `#!` que escrivim al principi d'un fitxer en entorns Unix/Linux.

Indica amb quin programa s'han d'interpretar les següents línies de l'script.

Aquí teniui uns quants exemples:
- [shebang0](shebang0)
- [shebang1](shebang1)
- [shebang2](shebang2)

Per executar un script doneu-li permisos d'execució. Per exemple:
```
chmod u+x shebang0
```
i després executeu-lo així:
```
./shebang0
```
