#!/bin/bash
# Filename:	EdicioAutomatitzada.sh
# Author:	vfornes
# Date:		09/03/2018
# Version:	0.1
# License:	This is free software, licensed under the GNU General Public License v3.
# 		See http://www.gnu.org/licenses/gpl.html for more information."
# Usage:	EdicioAutomatitzada.sh
# Description:	Script que demana un directori i modifica les fotos que hi hagi.

# Comprova que no hi hagi cap argument



# Demana el directori de les imatges originals
# No volem que zenity mostri missatges d'error



# Demana l'amplada per a les imatges i comprova que sigui un nombre enter positiu



# Estableix el nom base per a les imatges



# Inicialitza el comptador d'imatges



# Busca imatges JPEG en el directori, usant expressions regulars



# Mira si té l'extensió .JPEG o .JPG en majúscules o minúscules



# Crea el directori de destinació, si no existeix



# Incrementa el comptador



# Crea la nova imatge amb la mida canviada, el número en el centre i el nom nou



# Mostra la carpeta de les imatges noves amb el visor del GNOME

