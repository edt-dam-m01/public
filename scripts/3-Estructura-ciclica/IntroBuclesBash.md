## Introducció als bucles en bash scripting

Recordem que la repetició o iteració d'un procés és el que anomenem bucle o cicle i en el nostre estil de programació consisteix en repetir diverses vegades una sentència fins que una certa condició canviï d'estat (de `true` a `false` o a l'inrevés).

Al bash, com la gran majoria de llenguatges de programació disposem de diferents estructures per representar el procediment que representa aquesta estructura, ens centrarem en dues: `while` i `for`.

No veurem l'estructura `until`. Considerem que és redundant aprendre les dues estructures ja que until no ens aporta res si dominem `while`. De fet `while` és llegeix com "repeteix les següents instruccions MENTRE aquesta condició sigui certa" i en canvi `until` es llegiria com "repeteix les següents instruccions FINS QUE aquesta condició sigui certa". De totes maneres si algú vol estudiar-ho: man bash

### while

Com ja hem avançat, l'estructura while executa una sèrie de sentències (ordres en el nostre cas) mentre una certa condició sigui `true`. Aquesta definició general (que serveix per a molts llenguatges) l'apliquem al nostre llenguatge de _bash scripting_ recordant que l'expressió que avalua `while` és una ordre o llista d'ordres i que un `true` per a nosaltres vol dir errorlevel igual a 0 (en canvi qualsevol errorlevel diferent d 0 seria `false`)

```
while ordre1
do
    ordreA
    ordreB
    ordreC
    ...
done
```

O sigui que repetirem les ordres `ordreA, ordreB, ordreC ...` mentre `ordre1` retorni 
errorlevel `0` (`true`).

Anem a practicar

1. Fem un compte enrere (des de 8 per exemple)

   ```
   crono=8 
   while [ $crono -gt 0 ] 
   do 
      echo -en "\r$crono"
      crono=$((crono-1)) 
      sleep 1 
   done 
   echo -e "\rBoooom!"
   ```
	
2.  Mostrem els diferents arguments que li passem per la línia d'ordres.

   ```
   while [ $# -ne 0 ]   # o també  [ $# -gt 0] 
   do 
      echo $1 
      shift 
   done
   ```

   Una altra solució:

   ```
   arg_num=1 
   while [ $arg_num -le $# ] 
   do 
      eval echo \$$arg_num		#O bé, echo ${!arg_num} 
      arg_num=$(($arg_num+1))	#O bé, let arg_num++ o let arg_num+=1
   done
   ```
   
3. Fins que no escollim “s” o “n” com a resposta a una pegunta d'un read no sortim d'un bucle.

   ```
   while [ "$resposta" != "s" ] && [ "$resposta" != "n" ] 
   do 
      echo "escull 's' o 'n'" 
      read resposta 
   done
   ```

### for

Veiem ara l'estructura tipus `for`:

   ```
   for  variable in  col·lecció d'elements
   do
      ordre1
      ordre2
      ...
   done
   ```

Exemple:

```
for  numero  in   1 2 3 4 5
do
   echo “el número actual és $numero”
done
```

4. Compte enrere amb un bucle for (tipus foreach )
   Podem utilitzar l'ordre seq:   
   `man seq`

   ```
   sequencia=$( seq 10 -1 1 )   # seq FIRST INCREMENT LAST
   for i in $sequencia 
   do
      echo "número: $i"
   done
   ```

5. Amb un while mostreu les línies que es reben per la línia de comandes afegint la cadena: "Línia:""

   ```
   while read line
   do 
      echo "Línia: $line"
   done
   ```

   OBS: tant si passeu un fitxer per la línia d'ordres com a entrada estàndard com si no passeu res l'script funcionarà igualment.

6. Fem el mateix d'abans però mostrant a més el número de caràcters de la línia.
   ```
   while read line 
   do 
      echo "Línia: $line" 
      echo "$(echo -n $line|wc -m) caràcters" 
   done
   ```

7. Fem l'exercici 6, però amb un for: (*)

   ```
   for line in $( cat ) 
   do 
      echo "Línia: $line" 
      echo "$(echo -n $line|wc -c) caràcters" 
   done
   ```

En realitat no funciona bé, no fa el mateix que el nostre `while`. Si penses que sí que ho fa bé, potser és 
que no has fet servir cap línia amb més d'una paraula.
Que és el que passa ?
