## Edició de fotografies automatitzada

El programa `convert` és un editor d'imatges per línia d'ordres. Per defecte, no està instal·lat al Fedora. Quin paquet has d'instal·lar?

Has de fer un script (sense arguments) que  faci aquestes coses:

1. Demanarà, amb el `zenity`, un directori local. Trobes que cal comprovar si el valor introduït és realment un directori?
2. Demanarà, amb el `zenity`, una mida per a l'amplada de les imatges. Proposarà 600, de manera que l'usuari, si vol, només haurà de prémer el botó `Ok`. També comprovarà que el valor entrat és un nombre vàlid (enter positiu). Si no ho és, immediatament li'l tornarà a demanar, repetidament, fins que s'entri un nombre vàlid.
3. En aquest directori buscarà fotografies. Podria fer-ho amb els "magic numbers" (ordre `file`), però ho farà comprovant sí tenen les extensions `.JPG` o `.JPEG`; acceptarà majúscules i minúscules; així practicarem les expressions regulars.
4. Si hi ha fotografies en aquest directori i només si n'hi ha, crearà un subdirectori de nom el nombre d'amplada introduït, on posarà les imatges modificades. Intenta aconseguir-ho sense haver de buscar-les amb aquesta finalitat; és a dir, en el mateix moment que modifica les imatges com s'explica en els següents punts.
5. De cada fotografia, amb l'ajut del programa `convert`, farà una còpia a dins d'aquest subdirectori amb les tres modificacions següents:
6. La primera és que les farà de l'amplada requerida, sense perdre les proporcions.
7. (Opcional) La segona és que escriurà en la mateixa imatge un nombre correlatiu començant per 1.
8. La tercera és que canviarà el nom de manera que el nom nou tindrà tres parts:
   - La primera part serà el nom del directori on es troben, canviant els espais (si n'hi ha), per caràcters de subratllat "\_", seguit d'un guió "-".
   - La segona serà un nombre correlatiu, començant en 1.
   - Finalment, s'afegirà el literal ".jpeg", sense cometes.
9. Tingueu en compte que de vegades, els noms dels directoris o fitxers porten espais o altres caràcters que ens poden complicar l'script. Ha de funcionar bé en tots els casos.
10. Quin és el visor d'imatges per defecte del GNOME? Què significa el seu nom?
11. Finalment obrirà aquest visor mostrant les imatges acabades de crear.
