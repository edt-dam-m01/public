#!/bin/bash
# Fitxer: ex34_kp.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio:    script que determina si els número que se li passa com a paràmetre és parell o senar


# Si el nombre d'arguments és diferent de 1 mostrem missatge d'error, ajuda i surtim indicant l'error al sistema
if [ $# -eq 0 ]
then
    echo "Nombre d'arguments incorrecte"
    echo "Ús. $0 nombre... "
    exit 1
fi


while [ $# -gt 0 ]
do
    # Si l'argument no és nombre enter mostrem missatge d'error, ajuda i surtim indicant l'error al sistema
    echo $1 | grep '^[-+]\?[0-9]\+$' &>/dev/null
    if [ $? -ne 0 ]
    then
        echo "Han de ser nombres enters"
        exit 2
    fi

    # Si el nombre enter és parell mostrem un missatge indicant-ho
    if [ $(( $1%2 )) -eq 0 ]
    then
        echo "El nombre $1 és parell"
    # Altrament mostrem un missatge indicant que és senar
    else
        echo "El nombre $1 és senar"
    fi
    shift
done
