#!/bin/bash
# Fitxer: ex36_kp.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio:	Script que ens mostrarà per pantalla els fitxers i directoris que hi ha al directori actual. 
#			Però ho farà amb el següent format:
# 			Si amb la comanda ls obtenim la següent sortida
#			fit1   prog1   prog2   joc
#			Amb el nostre script ens ho mostrarà així:
#			|_fit1
#			.|_prog1
#			..|_prog2
#			...|_joc


# Creem una cadena de sortida inicial
cadena='|_'

# Anem agafant un a un els fitxers del directori actual
# Tinguem en compte que els noms dels fitxers poden contindre espais
ls | while read fitxer
     do
     # Per a cada fitxer creem la cadena de sortida i la mostrem
         echo "$cadena$fitxer" # concateno les dues cadenes
	 cadena=.${cadena} # Afegeixo un punt a la cadena, per al proper fitxer
     done
