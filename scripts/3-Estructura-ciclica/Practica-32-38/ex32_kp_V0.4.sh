#!/bin/bash
# Fitxer: ex32_kp_V0.4.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.4
# Descripcio: script que suma tots els números que se li passen per paràmetre
#		Fem control d'errors. Només acceptem enters (positius o negatius)


 

# Sumarem els nombres de la següent manera
# Al principi la nostra suma valdrà zero

suma=0
# Per a cada un dels arguments:
# si no és un número mostraré un missatge d'error i sortiré de l'script
# altrament afegiré, l'argument, a la suma anterior


while [ $# -gt 0  ] 
do
	echo $1 | grep '^[-+]\{0,1\}[0-9]\+$' > /dev/null
	
	if [ $? -ne 0 ]
	then
		echo "error: has introduit un argument que no és un número: $1 "
		exit 1
	fi
	suma=$((suma + $1))
	shift
done

# Mostraré el resultat

echo $suma

