#!/bin/bash
# Fitxer: ex33_kp_V0.2.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.2
# Descripcio: script que compta el número de caràcters que tenen els noms dels fitxers del directori actual.
#		Suposarem que tenim permís de lectura. No mirarem recursivament, és a dir només examinem al primer nivell de profunditat
#


# Capturarem tots els fitxers del directori actual

tots_fitxers=`ls`

# Per a cada fitxer del directori actual mostrem el numero de caracters que té el nom del fitxer


# Solució general, vàlida també si els noms dels fitxers tenen espais
ls | while read nomfitxer
do
    nombredelletres=$( echo -n "$nomfitxer" | wc -m )
    echo $nomfitxer : $nombredelletres
done


