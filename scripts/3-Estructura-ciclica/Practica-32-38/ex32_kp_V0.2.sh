#!/bin/bash
# Fitxer: ex32_kp_V0.2.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.2
# Descripcio: script que suma tots els números que se li passen per paràmetre
#		No fem control d'errors


 

# Sumarem els nombres d ela següent manera
# Al principi la nostra suma valdrà zero

suma=0
# Per a cada un dels arguments afegiré, l'argument, a la suma anterior

for i in `seq $#`
do
# Diferents formes de fer la suma:
#	eval argument=\$$i     # Mireu la nota(*)
#	suma=$((suma + $argument))
#	let suma+=$argument
	eval let suma+=\$$i
done

# Mostraré el resultat

echo $suma

# (*) Example 28-1. Indirect Variable References (http://tldp.org/LDP/abs/html/ivr.html)


