#!/bin/bash
# Fitxer: ex32_kp_V0.1.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio: script que suma tots els números que se li passen per paràmetre
#		No fem control d'errors


 

# Sumarem els nombres de la següent manera
# Al principi la nostra suma valdrà zero
suma=0
# Per a cada un dels arguments afegiré, l'argument, a la suma anterior

Resp="La suma de "

while [ $# -gt 0 ]       # Equivalentment [ "$1" != "" ]
do
	suma=$((suma+$1))
	Resp="${Resp}$1+"
	shift # Això em substitueix el argument2 a la posició argument1 i així succesivament, perdo el $1
done


# Mostraré el resultat

echo -e "$Resp\b és $suma"



