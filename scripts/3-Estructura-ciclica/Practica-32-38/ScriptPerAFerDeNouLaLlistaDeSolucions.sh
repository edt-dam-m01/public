#!/bin/bash

dir=$(dirname $0)

co="### Exercicis de la practica 32-39\n\n| Solucions |   |   |   |\n|---|---|---|---"

old=""
for s in $dir/ex*
do
   a=$(basename $s)
   if [[ $a =~ comments ]]
   then false
   else
      [ "${a:2:2}" != "${old:2:2}" ] && co="${co}|\n"
      co="${co}| [$a]($a) "
      old=$a
   fi
done

co="${co}|\n"

echo -e $co > $dir/Solucions.md

# zenity --info --text="Modifica, si cal el nombre de barres verticals de les dues primeres línies de la taula";vim $dir/Solucions.md

firefox $dir/Solucions.md
