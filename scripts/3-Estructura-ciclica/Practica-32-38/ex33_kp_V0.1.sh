#!/bin/bash
# Fitxer: ex33_kp_V0.1.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio: script que compta el número de caràcters que tenen els noms dels fitxers del directori actual.
#		Suposarem que tenim permís de lectura. No mirarem recursivament, és a dir només examinem al primer nivell de profunditat
#



# Solució per a fitxers sense espais en el seu nom
for nomfitxer in `ls`     # Capturarem tots els fitxers del directori actual
do
    # Per a cada fitxer del directori actual mostrem el numero de caracters que té el nom del fitxer
    nombredelletres=$( echo -n $nomfitxer | wc -m )
    echo $nomfitxer : $nombredelletres caràcters
    # En una sola línia:
    # echo $nomfitxer : $( echo -n $nomfitxer | wc -m ) caràcters
done


