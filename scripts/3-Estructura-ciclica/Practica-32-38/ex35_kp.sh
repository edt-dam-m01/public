#!/bin/bash
# Fitxer: ex35_kp.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio: script que demani un nom d'usuari i ens digui si aquest existeix i, si existeix, ens digui si està conectat


# Demanem el nom d'usuari i l'emmmagatzemem
read -p "Escriu un nom d'usuari: " nom_usuari

# Si no ens entren cap valor mostrem missatge d'error i sortim indicant l'error al sistema
if [ -z $nom_usuari ]
then
    echo "No has escrit cap nom"
    exit 1
fi

# Altrament si l'usuari no existeix mostrem un missatge indicant-lo i sortim del programa
cat /etc/passwd | cut -f1 -d':' | egrep -w $nom_usuari &> /dev/null
#cat /etc/passwd | egrep "^$1:" &> /dev/null
if [ $? -ne 0 ]
then
    echo "$nom_usuari no és un usuari del sistema"
    exit 2
fi

# En cas contrari, si l'usuari està conectat mostrem un missatge indicant-lo
who | grep "^$nom_usuari " &> /dev/null
if [ $? -eq 0 ]
then
    echo "L'usuari $nom_usuari està connectat"
# Si no, el missatge dirà que no està connectat
else
    echo "L'usuari $nom_usuari no està connectat"
fi
