#!/bin/bash
# Fitxer: ex34_kp_comments.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio:    script que determina si els número que se li passa com a paràmetre és parell o senar


# Si el nombre d'arguments és diferent de 1 mostrem missatge d'error, ajuda i surtim indicant l'error al sistema

# Si l'argument no és nombre enter mostrem missatge d'error, ajuda i surtim indicant l'error al sistema

# Si el nombre enter és parell mostrem un missatge indicant-ho

# Altrament mostrem un missatge indicant que és senar


