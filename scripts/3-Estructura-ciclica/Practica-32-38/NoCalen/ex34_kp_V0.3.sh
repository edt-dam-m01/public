#!/bin/bash
# Fitxer: ex34_kp_V0.3.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.3
# Descripcio:    script que determina si els números que se li passen com a paràmetres són parells o senars. Utilitza una funció


# funció que retorna 0 si el numero é parell i 1 si és senar
es_parell() {
	if [ $(($1%2)) -eq 0 ]
	then
        	return 0
	else
        	return 1
	fi
}

# Si el nombre d'arguments és zero mostrem missatge d'error, ajuda i surtim indicant l'error al sistema
if [ $# -eq 0 ]
then
	echo "Has d'entrar més d'un argument"
	echo "ús: $0 número1 [numero2 ...] "
	echo "    on número1 [,numero2...] són nombres enters"
	exit 1
fi

# Per a cada argument
while [ $# -gt 0 ]
do
	# Si l'argument no és nombre enter mostrem missatge d'error, ajuda i surtim indicant l'error al sistema
	echo $1 | grep '^[-+]\{0,1\}[0-9]\+$' > /dev/null
	if [ $? -ne 0 ]
	then
		echo "error: has introduit un argument que no és un número enter: $1 "
	# Si el nombre enter és parell mostrem un missatge indicant-ho
	elif es_parell $1
	then
		echo "$1 és parell"
	# Altrament mostrem un missatge indicant que és senar
	else
		echo "$1 és senar"
	fi
	shift
done
