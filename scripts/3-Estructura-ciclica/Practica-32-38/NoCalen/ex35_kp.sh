#!/bin/bash
# Fitxer: ex35_kp.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio: script que demani un nom d'usuari i ens digui si aquest existeix i, si existeix, ens digui si està conectat


# Demanem el nom d'usuari i l'emmmagatzemem
echo "Sisplau introdueix el login de l'usuari per saber si està connectat"
read USUARI


# Si no ens entren cap valor mostrem missatge d'error i sortim indicant l'error al sistema

if [ -z "$USUARI" ]
then
	echo "no has entrat res"
	exit 1
fi

# Altrament si l'usuari no existeix mostrem un missatge indicant-lo i sortim del programa
cat /etc/passwd | grep "^$USUARI:" &> /dev/null
if [ $? -ne 0 ]
then
	echo "l'usuari $USUARI no existeix"
fi	

# En cas contrari, si l'usuari està conectat mostrem un missatge indicant-lo

who |  cut -d' ' -f1 | grep "$USUARI" &> /dev/null
if [ $? -eq 0 ]
then
	echo "$USUARI està connectat"
else
	# Si no, el missatge dirà que no està connectat
	echo "$USUARI no està connectat"
fi

