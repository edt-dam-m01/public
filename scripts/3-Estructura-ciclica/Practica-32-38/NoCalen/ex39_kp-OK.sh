#!/bin/bash
# Fitxer:	ex39_kp.sh
# Autor:	vfornes
# Data:		14/03/2018
# Versio: 0.1
# Descripcio:	Script que permet copiar (-c), moure (-m) o esborrar (-d) fitxers.
#		L'script ha de comprovar que la sintaxi utilitzada és la correcta.


# Observacions generals:
# Només permetrem que l'opció (-c, -m, -d) sigui el primer argument
# Es permet qualsevol nombre d'arguments sempre que sigui més gran que 2 per -d i més gran que 3 als altres casos.
# Farem una funcio per a cada una de les 3 accions.
# Tot i que no ho diu per motius de seguretat quan ens diguin d'esborrar directoris no ho farem, ni tan sols ho controlarem
# Tampoc no controlem que els fitxers existeixin


# funció esborra
esborra () {
	# Si el nombre d'arguments és zero, ja hem descomptat l'argument -d, enviem un error al sistema
	if [ $# -eq 0 ]
	then
		return 1
	fi
	
	# Comprovem si tots els arguments passats són fitxers regulars
	for fitxer in $@
	do
		if ! [ -f $fitxer ]
		then return 2
		fi
	done

	# En cas afirmatiu, esborrem tots els fitxers
	rm -i $@

	return 0
}


# funció copia
copia () {

	# Si el nombre d'arguments és més petit que 2, ja haurem descomptat l'argument -c, enviem un error al sistema
	if [ $# -lt 2 ]
	then
		return 1
	fi

	ultim_argument=${!#}     # Trobem l'últim argument passat amb expansió indirecta

	# Altrament, si el nombre d'arguments és superior a 2 i el darrer argument no és un directori enviem un error al sistema.
	if [ $# -gt 2 ] && ! [ -d $ultim_argument ]
	then
		return 2
	fi
	
	# En cas contrari copiem tots els fitxers menys l'últim cap a l'últim, ja que si hi ha més de dos arguments, l'últim és un directori
	cp $@

}




# funció mou
mou () {

   # Si el nombre d'arguments és més petit que 2, ja haurem descomptat l'argument -m, enviem un error al sistema
    if [ $# -lt 2 ]
    then
        return 1
    fi

    ultim_argument=${!#}     # Trobem l'últim argument passat amb expansió indirecta

    # Altrament, si el nombre d'arguments és superior a 2 i el darrer argument no és un directori enviem un error al sistema.
    if [ $# -gt 2 -a ! -d $ultim_argument ]
    then
        return 2
    fi

	# En cas contrari movem tots els fitxers menys l'últim cap a l'últim, ja que si hi ha més de dos arguments, l'últim és un directori
	mv $@

	# Aqui podriem diferenciar en el cas de que hi hagin 2 arguments i el segon sigui un fitxer que existeix, però no ho farem
}


# Si el primer argument no és una de les 3 opcions, mostrem un missatge d'error i informem al sistema de l'error
# Altrament enviem els arguments a la funció corresponent perquè s'executi
if [ "$1" = "-d" ]
then
	shift
	esborra "$@"
elif [ "$1" = "-m" ]
then
	shift
	mou "$@"

elif [ "$1" = "-c" ]
then
	shift
	copia "$@"
else
	echo "error: el primer argument no és correcte"
	echo "ús: $0 -d|-c|-m argument1 argument2 [ argument3[...]]"
	exit 1
fi



# Si la funció ha fallat mostrem un missatge d'error i informem al sistema
error_funcio=$?
if [ $error_funcio -eq 0 ]
then
	#no fem res tot ok
	exit 0
elif [ $error_funcio -eq 1 ]
then
	echo "nombre d'arguments incorrecte per a l'operació"
	exit 2
else
	echo "Tipus d'arguments incorrectes."
	echo "Només es poden eliminar fitxers regulars"
	echo "Per a copiar/moure més d'un fitxer es necessita que l'últim argument sigui directori"
	exit 3
fi







<<*
NOTA: A l'últim bloc, on controlem el valor de retorn de la funció hi ha una errada important. Trobeu-la

SOLUCIÓ: Per veure la solució copieu i executeu en una consola el següent paràgraf (després llegiu acuradament):
			clear; echo "CGm qHZ '\$?' és Zl vXlGr dZ rZtGrn dZ l'últVmX cGmXndX ZxZcHtXdX, pZr Xl prVmZr 'Vf' sí qHZ fHncVGnX: és X dVr l'ZxZcHcVó dZ lX fHncVó és jHst l'GrdrZ XntZrVGr. PZrò qHXn XrrVbX Xl 'ZlVf' Zl sZgüZnt '\$?' jX nG fX rZfZrèncVX X l'ZxZcHcVó dZ lX fHncVó. AqHZstX vZgXdX lX dXrrZrX GrdrZ és Hn tZst '[ \$? -Zq 0 ]' V pZr tXnt sV XrrVbX Xl ZlVf és pZrqHè Zl tZst hX ZstX 'fXls' V pZr tXnt té Hn ZrrGrlZvZl dZ 1." | tr 'XZVGH' 'aeiou'
*



