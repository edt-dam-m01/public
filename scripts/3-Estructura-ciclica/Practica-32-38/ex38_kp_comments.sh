#!/bin/bash
# Fitxer: ex38_kp_comments.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio: script que suma les mides de tots els fitxers del directori passat com a argument,
#		donant un missatge d'error si l'argument passat no és un directori o no existeix.
#		Aquest script necessita tenir al mateix directori l'script 37. 
#		No controlem si aquest script hi és.

# Suposarem el següent: 
#	Li estem proporcionant la ruta, ja sigui absoluta o relativa, per tant no ha de fer búsquedes.
# 	Mostrarem error per si el fitxer no és un directori o no existeix.
#	Només calcularem les mides dels fitxers que siguin regulars



# Si no passem un únic argument mostrem un missatge d'error i sortim informant de l'errada al sistema

# Altrament, si no existeix mostrem un missatge d'error i sortim informant de l'errada al sistema

# Altrament, si no és un directori mostrem un missatge d'error i sortim informant de l'errada al sistema

# Si no, executem l'script anterior (ex37) per als fitxers continguts al directori












