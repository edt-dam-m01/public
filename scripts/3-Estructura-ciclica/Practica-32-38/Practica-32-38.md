## Pràctica scripts 32 a 38

Heu de lliurar tots els scripts del número 32 al 38 (ambdós inclosos), l'enunciat dels quals
trobareu al document PDF "Curs de shell scripting", pàgina 35, que hi ha en "Recursos bash scripting" 
en el curs de Moodle.

Es recomana fer en aquest ordre les següents tasques:

```
1. Capçalera (gairebé automàtica), amb l'script header.sh.
2. Descripció.
3. Comentaris que seran la base d'una bona codificació de l'script.
4. Només després de fer els 3 anteriors codifiqueu bash script.
```

Per comprovar que heu fet be els 3 primers passos, al final d'aquest document teniu les solucions parcials 
   on surten fets aquests 3 passos. És per veure si coincideixen amb els vostres comentaris, per tant
   sis plau no el llegiu fins que escriviu els vostres comentaris.

En el cas de que els vostres comentaris descriguin un proces diferent, feu l'script escollint la
   manera que més us agradi, tot i que si teniu temps l'ideal seria que els féssiu de les dues formes.
   
Optativament, quan acabeu, feu l'script número 40.



### Solucio parcial (nomes comentaris) a la pràctica 32-39

- [ex32_kp_comments.sh](ex32_kp_comments.sh)
- [ex33_kp_comments.sh](ex33_kp_comments.sh)
- [ex34_kp_comments.sh](ex34_kp_comments.sh)
- [ex35_kp_comments.sh](ex35_kp_comments.sh)
- [ex36_kp_comments.sh](ex36_kp_comments.sh)
- [ex37_kp_comments.sh](ex37_kp_comments.sh)
- [ex38_kp_comments.sh](ex38_kp_comments.sh)
