#!/bin/bash
# Fitxer: ex32_kp_V0.3.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.3
# Descripcio: script que suma tots els números que se li passen per paràmetre
#		No fem control d'errors


 

# Sumarem els nombres de la següent manera
# Al principi la nostra suma valdrà zero
suma=0
# Per a cada un dels arguments afegiré, l'argument, a la suma anterior

for i in $@
do
    (( suma+=i ))
done

# Mostraré el resultat

echo -e "La suma és $suma"



