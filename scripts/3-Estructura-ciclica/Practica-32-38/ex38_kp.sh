#!/bin/bash
# Fitxer: ex38_kp.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio: script que suma les mides de tots els fitxers del directori passat com a argument,
#		donant un missatge d'error si l'argument passat no és un directori o no existeix.
#		Aquest script necessita tenir al mateix directori l'script 37. 
#		No controlem si aquest script hi és.


# Suposarem el següent: 
#	Li estem proporcionant la ruta, ja sigui absoluta o relativa, per tant no ha de fer búsquedes.
# 	Mostrarem error per si el fitxer no és un directori o no existeix.
#	Només calcularem les mides dels fitxers que siguin regulars



# Si no passem un únic argument mostrem un missatge d'error i sortim informant de l'errada al sistema
if [ $# -ne 1 ]
then
	echo "Error en el nombre d'arguments"
	echo ús: $0 directori
	exit 1
fi


# Altrament, si no existeix mostrem un missatge d'error i sortim informant de l'errada al sistema
if [ ! -e $1 ]
then
	echo "Error: $1 no existeix"
	echo "Repassa la sintaxi o la trajectòria"
	exit 2
fi


# Altrament, si no és un directori mostrem un missatge d'error i sortim informant de l'errada al sistema
if [ ! -d $1 ]
then
    echo "Error: $1 no és un directori"
    exit 3
fi

# Si no, executem l'script anterior (ex37) per als fitxers continguts al directori

bash ex37_kp.sh $1/*










