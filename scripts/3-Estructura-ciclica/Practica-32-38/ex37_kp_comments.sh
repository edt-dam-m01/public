#!/bin/bash
# Fitxer: ex37_kp_comments.sh
# Autor: vfornes
# Data: 14/03/2018
# Versio: 0.1
# Descripcio: script que suma les mides de tots els fitxers que se li passen com a arguments
#			donant un error per a tots aquells arguments que no existeixin
#			o que siguin directoris.


# Suposarem el següent: 
#	Li estem proporcionant la ruta, ja sigui absoluta o relativa, per tant no ha de fer búsquedes.
#	Enviarem l'error a stderr
# 	Mostrarem error per a tots els fitxers que no siguin regulars, no només directoris


# Inicialitzo a zero una variable acumuladora on tindré les sumes parcials de les mides dels fitxers

# Mentre hi hagi arguments, emmagatzemo el primer argument

	# Si no el trobo, mostro missatge d'error
	# altrament, si no és un fitxer regular, mostro missatge d'error
	# altrament, calculo la seva mida i l'afegeixo al total


# Mostro el total





