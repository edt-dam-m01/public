1) Mostreu tots els directoris de /tmp (si no es mostren errors fer-ho amb /var o /root, és a dir algun directori que no tingueu permisos)

[jamoros@pc93 ~]$ ls /root
ls: cannot open directory /root: Permission denied


2) Xequejeu amb alguna ordre si hi hagut alguna errada.

[jamoros@pc93 ~]$ echo $?
2


3) L'error estàndard es pot redireccionar amb "2>". 
D'altra banda, els missatges que no volem que apareguin els podem enviar a /dev/null
(Ull viu, no estem dient que els missatges d'error no serveixin !) 
Feu una prova ara.


[jamoros@pc93 ~]$ ls /root 2> /dev/null 


4) Xequejeu ara si ha hagut error.

[jamoros@pc93 ~]$ echo $?
2

5) Ara volem perdre de vista els missatges d'error però la sortida estàndard la volem enviar a un fitxer "sortida.txt".

[jamoros@pc83 ~]$ ls /var/ /root/ 2> /dev/null > sortida.txt

Comprovem

[jamoros@pc83 ~]$ cat sortida.txt 
/var/:
account
adm
cache
db
empty
games
gdm
gopher
lib
local
lock
log
mail
nis
opt
preserve
run
spool
tmp
yp



6) Ara volem fer el mateix d'abans però, a més a més, que surti per pantalla.

[jamoros@pc83 ~]$ ls /var/ /root/ 2> /dev/null | tee sortida.txt
/var/:
account
adm
cache
db
empty
games
gdm
gopher
lib
local
lock
log
mail
nis
opt
preserve
run
spool
tmp
yp


7) Com es podria desar en algun fitxer els missatges d'error, fent  el que s'ha fet a l'apartat anterior?

[jamoros@pc83 ~]$ ls /var/ /root/ 2> error.txt | tee sortida.txt
/var/:
account
adm
cache
db
empty
games
gdm
gopher
lib
local
lock
log
mail
nis
opt
preserve
run
spool
tmp
yp

Si mirem el fitxer error.txt

[jamoros@pc83 ~]$ cat error.txt 
ls: cannot open directory /root/: Permission denied


