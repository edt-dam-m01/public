Enllaç extern sobre redireccions: [http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-3.html](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-3.html)

Nota: Recordeu que "stdout 2 file" és una expressió en anglès on s'utilitza "2" en comptes de "to" ja que el so és el mateix. D'altres expressions semblants serien:
4 U = for you
C U = see you
...

OBS: Altres enllaços molt interessants són
- [http://www.catonmat.net/blog/bash-one-liners-explained-part-one/](http://www.catonmat.net/blog/bash-one-liners-explained-part-one/)
- [http://www.catonmat.net/blog/bash-one-liners-explained-part-two/](http://www.catonmat.net/blog/bash-one-liners-explained-part-two/)
- [http://www.catonmat.net/blog/bash-one-liners-explained-part-three/](http://www.catonmat.net/blog/bash-one-liners-explained-part-three/)
