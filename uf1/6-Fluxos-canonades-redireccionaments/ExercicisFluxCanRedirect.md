### Exercicis fluxos, canonades i redireccions

1. Anem a revisar certs conceptes que ja hauríem de dominar.
	
	1. Mostreu tots els fitxers dels directoris de `/lost+found` o `/root`, de
	   manera que ens surti algun error perquè no tinguem permisos.

	2. Xequejeu amb alguna ordre si hi ha hagut algun error.

	3. Amb quina ordre podem fer que no es mostrin els missatges d'error, per exemple de `ls /root`?
	   (Atenció!, no estem dient que els missatges d'error no serveixini de res!

	4. Com podem comprovar ara si ha hagut error?

	5. Ara volem perdre de vista els missatges d'error però la sortida
	   estàndard la volem enviar al fitxer _sortida.txt_. Haureu de llistar un directori que hi 
           tingueu permissos i un altre que no.

	6. Ara volem fer el mateix d'abans però, a més a més, que el missatge de
	   l'entrada estàndard surti per pantalla.

	7. El mateix d'abans però emmagatzemant els missatges d'error a un altre fitxer?
	
	8. Volem cercar certa informació només de la sortida estàndard de l'error,
	   la resta no m'interessa gens. Ho fem de la següent manera:
	
		```
		ls /root /var /lost+found > /dev/null 2> sortida.txt
		cat sortida.txt | grep 'patró a cercar'
		```
	
		És possible fer-ho sense el fitxer temporal sortida.txt?
		
		**Hint: redireccions.**

2. Ja sabem que tot i que semblants, hi ha diferències substancials quan
   executem les següents instruccions:
	```
	find ....   -exec ordre '{}'  \;
	find ....  | xargs ordre
	```
	- A la primera instrucció quan troba la primera ocurrència li passa
	aquesta ocurrència a l'ordre que té d'argument `-exec` perquè s'executi, després
	cerca la següent ocurrència i quan la troba li la torna a passar a l'ordre.
	- En canvi amb `xargs`, troba totes les ocurrències de cop i després li
	les passa a l'ordre.

	Si pensem en eficiència està clar que la 2a opció és millor que la 1a. De fet
	seria semblant a preguntar-nos quina de les següents ordres és més ràpida:

	```
	ls /dir1; ls /dir2; ls /dir3; ls /dir4; ls /dir5; ls /dir6; ls /dir7
	ls /dir1  /dir2  /dir3  /dir4  /dir5  /dir6  /dir7
	```

	Intuïtivament sembla clar que és més eficient cridar a una ordre una vegada
	i passar-li diferents paràmetres en comptes de cridar a la mateixa ordre
	moltes vegades.  Comproveu això últim posant l'ordre `time` davant de cada
	ordre per veure quan de temps triga (quan l'ordre és _composta_ podeu posar
	unes claus amb `;` al final `{ ...; }`).

	_xargs_ te d'altres problemes amb el tractament de cadenes com
	se us explica als apunts d'IBM.

	1. Si busqueu una mica, al _man_ per exemple, trobareu que per a
	   versions modernes de `find` podem aconseguir que `-exec` actuï com
	   al `xargs`, és a dir que primer es trobin totes les ocurrències i
	   que després se li passin a l'ordre per executar una única vegada.
	   Trobeu com s'ha de fer això.
	 
	2. Als apunts de LPI d'IBM se us explica que si volem utilitzar la
	   sortida d'una ordre o el contingut d'un arxiu com a argument d'una
	   altre ordre les canonades (pipes) no funcionen i que disposem
	   d'almenys 3 mètodes.  Practiquem una mica.  Expressa la següent
	   ordre, que utilitza substitució de comandes, amb `find ... -exec` i
	   amb `find | xargs`:
	  
		```
		more $(ls /etc/*.conf)
		```

3. Hem rebut un vídeo d'un usuari anònim on es veu com treballa amb el bash.
   Ens repta a que esbrinem quina ordre va utilitzar inicialment per aconseguir
   el que es veu al vídeo [missatges_perduts.ogv](missatges_perduts.ogv)

4. Volem tornar a fer l'script _header.sh_ però en comptes de fer servir els `echo` per enviar línies al nou fitxer script farem servir els `here-document`.

	1. Creeu un script molt senzill, que enviï a un cert fitxer, per exemple
	   file1, una certa cadena, per exemple "Hola. Què tal?" mitjançant
	   _here-document_.

	2. Modifiqueu l'script _header.sh_, de manera que faci el mateix però
	   utilitzant els _here-document_. Agafeu la darrera versió que teniu al
	   moodle i canvieu només el que sigui necessari.

Com sempre utilitzareu els apunts d'IBM i també podeu fer un cop d'ull a <http://tldp.org/LDP/abs/html/here-docs.html> 

![/dev/null](dev_null.jpeg)
