#### Diving exec: l'ordre exec amb arguments o sense

D'una banda tenim que podem utilitzar l'ordre `exec` amb arguments que serien
ordres, així com els arguments de l'ordre `ls` són fitxers i/o directoris:

```
exec ordre1
```

Aquí el que fa `exec` és executar `ordre1` però la manera com s'executa és especial. En comptes d'executar-se com un nou procés del shell actual, substitueix el procés shell actual pel procés *ordre1*.

Aprofundir més sobre l'ordre `exec` ens porta a conceptes com *fork* que sobrepasen de llarg l'abast de la nostra assignatura. Tot i així a sota teniu un enllaç per veure els possibles usos d'exec a un script avançat, per si és del vostre interès.

D'altra banda podeu executar l'ordre **sense** arguments `exec` de la mateixa manera que podeu executar `ls` que llavors és equivalent a `ls $PWD`

* `exec` sense arguments, o sigui sense ordres, i res més, no fa res.
* `exec` sense arguments, o sigui sense ordres, però amb redireccions sí que fa *coses*.

Tot i que sembli curiós, el fet de no tenir arguments no vol dir que no es puguin fer redireccions.

Per exemple, jo puc redireccionar la sortida estàndard només per a una ordre:

```
ls 1>file1
```

però després d'aquesta ordre la sortida estàndard tornarà a ser la que era abans, per exemple, la pantalla.

L'ordre anterior es pot escriure en una disposició diferent i fa exactament el mateix:

```
1>file1 ls
```

Aquesta última ordre la podem llegir com *canvia la sortida estàndard actual pel fitxer file1
però **només** per a l'ordre que escrigui a continuació* (en aquest cas ```ls```)

Però i si volgués que aquesta redirecció fos vàlida, no per una única ordre, sinó per **totes** les ordres següents mentre no surti de la sessió de shell actual, o no es digués el contrari amb un altra ordre?

Doncs la solució la dona `exec` sense arguments (però òbviament amb redireccions):
```
exec 1>file1
```

Mentre no surtim de la shell on s'ha executat aquesta ordre o s'escrigui el contrari (per exemple `exec 1> $(tty)`) o s'acabi d'executar l'script on està l'ordre, la sortida de totes les ordres aniran a parar al fitxer _file1_.

#### Links:

* [Font original d'on surt aquest text](http://www.tldp.org/LDP/abs/html/x17974.html)

* [Usos d'_exec_ als scripts](http://stackoverflow.com/questions/18351198/what-are-the-uses-of-the-exec-command-in-shell-scripts?answertab=votes#tab-top)
