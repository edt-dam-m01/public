## Exercici final de gestió de disc

Fa uns temps indeterminat es va crear una petita partició de 100 MB amb un sistema de fitxers *ext4*. 

Després de crear i eliminar fitxers en aquesta partició, es va decidir fer una imatge (d'aquesta partició) per si de cas es volia muntar en algun altre dispositiu o punt de muntatge. 

Com que la partició tenia molt d'espai lliure la imatge resultant, que tenia la mateixa mida de 100 MB, es podia comprimir molt: [partition_100M.xz](partition_100M.xz)

##### Exercici

A la partició original hi ha un fitxer amb un missatge important, heu de llegir el seu contingut (i tenir curiositat).

Podeu demanar pistes, tot i que no té tant de mèrit com aconseguir resoldre el trencaclosques sols. 

##### Solució

- Descarrega el fitxer partition_100M.xz
- Descomprimeix-lo
   ```
   man xz
   xz -d partition_100M.xz
   ```
- Associa-li un dispositiu loop
   ```
   man losetup
   # losetup /dev/loop0 partition_100M
   ```
- Munta aquest dispoditiu per tal de poder veure el contingut
   ```
   # mkdir /mnt/p100M
   # mount /dev/loop0 /mnt/p100M
   ```
- Llegeig el fitxer `dontreadme.md` que hi ha a dins
   ```
   ls /mnt/p100M
   vim /mnt/p100M/dontreadme.md
   ```
- Quina pista ens dóna aquest fitxer? Només la paraula *undelete*... i que la partició en qüestió té el sistema de fitxers ext4... Busquem una mica... "undelete ext4"... ... ... ... ... Premi: *extundelete* !!!!
- L'instal·lem
   ```
   # dnf install extundelete
   ```
- Lamentablement, *extundelete* no té pàgina de manual
   ```
   man extundelete
   extundelete --help | less
   # extundelete --restore-all /dev/loop0
   ...però també es podia treballar directament amb la imatge, i aleshores, no cal ser `root`
   extundelete --restore-all partition_100M
   ```
- Sembla es resultat de `split`. Però hi ha un fitxer `file.12` que deuria ser `xaa`
   ```
   mv file.12 xaa
   ```

- Com es desfà un `split`?
   ```
   cat x*
   o, si ho preferim en un fitxer, en lloc de veure-ho per pantalla,
   cat x* > Quijote.txt
   ```

Hem obtingut 89 MB de Quijote!
