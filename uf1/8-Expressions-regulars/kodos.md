## Editor gràfic d'expressions regulars (phyton)

El programa **Kodos** ens permet practicar amb comoditat amb les expressions regulars.

![Kodos](kodos.png)

A Fedora i altres GNU/Linux el podem instal·lar des dels dipòsits de la distribució.

```
dnf install kodos
```

