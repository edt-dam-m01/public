## Exercicis d'expressions regulars amb sed i grep

Respongueu amb aquest mateix document, afegint les ordres que es busquen en cada exercici.

### Exercici 1

Del resultat de fer un head de les 15 primeres línies del fitxer `/etc/passwd`
mostraeu les línies que contenen un 2 en algun lloc.

### Exercici 2

Del resultat de fer un head de les 15 primeres línies del fitxer `/etc/passwd`
mostreu només les línies que tenen *uid* 2.

### Exercici 3

Usant grep valideu si un dni té el format apropiat.

### Exercici 4 

Usant grep valideu si una data té un format vàlid. Els formats poden ser: dd-mm-aaaa o dd/mm/aaaa.

### Exercici 5

Per als següents exercicis, cal utilitzar el fitxer [noms1.txt](noms1.txt)

Busqueu totes les línies del fitxer noms1.txt que tenen la cadena *Anna* o la cadena *Jordi*

### Exercici 6

Substituïu del fitxer noms1.txt tots els noms Anna i Jordi per -nou-.

### Exercici 7

Obtingueu les 10 primeres línies del fitxer `/etc/passwd` substituint `/sbin/nologin` per `-noshell`.

### Exercici 8

Ídem que l'exercici anterior però fent la substitució només de les línies 4 a la 8.

### Exercici 9

Ídem exercici anterior però fent les substitucions des de la línia que conté *adm* fins la línia que conté *halt*.

