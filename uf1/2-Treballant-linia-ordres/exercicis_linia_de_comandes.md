### Exercicis fets amb Fedora 24 (octubre 2016)


1. Responeu a la pregunta sense provar-ho a la terminal. Que es mostrarà per pantalla si executo:

	```
	[leviatan@pc666 ~]$ echo -n "mòdul 01" || echo "UF1" && echo -e "\nSistemes Informatics"
	```

2. Executa només la primera ordre i endevina què mostrarà la segona:
	```
	echo $BASH_SUBSHELL
	(sleep 1; echo $BASH_SUBSHELL; sleep 1)
	```

3. Feu el listing 9 afegint una ordre "sleep 10" al final, de manera que es llegeixi:

	```
	[leviatan@pc666 ~]$ bash -c "echo Expand in parent $$ $PPID; sleep 10"
	```
	Abans de que no s'esgotin els 10 segons executeu en una altra terminal la següent ordre:
	```
	pstree -pha
	```
	Mireu de trobar el procés *sleep*

4. Estic al bash i he executat:
	```
	[leviatan@pc666 ~]$ animal="cat"
	```
	o potser
	```
	[leviatan@pc666 ~]$ animal="dog"
	```
	de fet no ho recordo bé, però vull que em mostri per pantalla el plural en anglès de l'animal que hi hagi a la variable.
	Quina ordre executaries?

5. Si obro una terminal i escric:
	```
	[leviatan@pc666 ~]$ exec sleep 10
	```
	que succeeix? ho podries explicar?

	*Històric*
6. Dona una ordre amb la qual pugui veure la meva arquitectura (bé, de fet la del meu pc). I la del nucli? i si vull una info completa del sistema (un breu resum)?

7. Quantes línies es desen a l'històric d'ordres a memòria? Troba la variable d'entorn que conté aquesta informació, o sigui el número de línies de l'històric a memòria. (hint: history, apropos, help ...)

8. I al fitxer històric d'ordres? Troba la variable d'entorn que conté aquesta informació (el número de línies del fitxer històric).

9. Es desen les ordres consecutives duplicades a l'històric de comandes? Quina variable conté aquesta informació? Doneu una solució fàcil perquè sí que emmagatzemi les ordres consecutives repetides.
Intenteu esbrinar a quin fitxer es troba aquesta variable. 

10. Amb quina combinació de tecles faig una cerca inversa i puc trobar la darrera vegada que he utilitzat una ordre que conté un cert patró? Com trobo l'anterior ocurrència?

11. En aquest exercici no heu de resoldre res, només executar les ordres que s’enumeren, però si no feu el següent exercici que resol el que heu fet en aquest, no podreu accedir al programa java de veritat i per tant el DrJava que utilitza aquesta ordre java no funcionarà.

	Feu:
	```
	echo $PATH
	```
	Se us mostren una sèrie de directoris separats pel caràcter ":"

	Per exemple una sortida podria ser:
	```
	/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/home/leviatan/.local/bin:/home/leviatan/bin
	```
	*ALERTA: Aquest exercici només funciona si al 1er directori del vostre PATH no es troba un executable de nom java.*

	Com a root fem el següent:
	```
	[root@pc666 ~]# vim /usr/local/bin/java
	```
	i dintre escrivim les següents línies:
	```
	#!/bin/bash
	# Script que mostra la data
	date
	```
	Tanquem i desem el fitxer. Com a root donem permís d'execució:
	```
	[root@pc666 ~]# chmod +x /usr/local/bin/java
	```
	I ara tornem a l'usuari ordinari iamxxxxxx i executeu:
	```
	[iamxxxxxx@pc666 ~]$ java
	```
12. Arreglem el problema creat abans.
	L'únic que he de fer és eliminar el fitxer que vaig crear abans.
	Un cop l'hem eliniat, comprovem per si de cas que l'hem eliminat bé, executant l'ordre:
	```
	[leviatan@pc666 ~]$ java
	```
	hauria de sortir:
	```
	...
	See http://www.oracle.com/technetwork/java/javase/documentation/index.html for more details.
	```
13. Ara treballaré amb un àlies:
	```
	[leviatan@pc666 ~]$ alias java="echo Java és una illa d'Indonèsia"
	```
	executeu ara:
	```
	[iamxxxxxx@pc666 ~]$ java
	```
	És necessari eliminar aquest alies? Tant si ho és com si no, digues com s'elimina un alies:
	```
	[iamxxxxxx@pc666 ~]$  unalias java
	```
	Si vull que un àlies funcioni sempre que hauria de fer?

	Desar-ho a un fitxer d'inicialització. El més habitual és ```~/.bashrc```

14. Suposem que volem instal·lar-nos el CD de F24 64 bits net-install des d'[aquest enllaç](http://archives.fedoraproject.org/pub/archive/fedora/linux/releases/24/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-24-1.2.iso)
i un cop hem baixat la iso volem asegurar-nos que ha estat ben baixada.

	Al mateix directori que conté la iso trobem el fitxer:

	Fedora-Workstation-24-1.2-x86_64-CHECKSUM

	i dintre la línia:
	```
	SHA256 (Fedora-Workstation-netinst-x86_64-24-1.2.iso) = 1d0aaa3dadba90bbac211729c406072dd91f67c04315effb50fd8d9e0aa8f6c0
	```
	El nostre sistema tindra alguna ordre relacionada amb aquest SHA256 per calcular el checksum

	Investigueu amb quina ordre podem calcular aquesta cadena per veure si ens hem baixat be la iso. (hint: apropos)
