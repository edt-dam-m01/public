# Exercicis sobre processos

### Exercici 1

Explica què fan aquestes ordres (què mostren):

```
$ ps
```

```
$ ps aux
```

```
$ top
```

```
$ pstree
```

```
$ nice sleep 100 &
```

```
$ renice 5 <PID>
```

### Exercici 2

Per què serveix l'ordre `kill`?

Explica què fan aquestes ordres:

```
$ kill -HUP <PID>
```

```
$ kill -TERM <PID>
```

```
$ kill -STOP <PID>
```

```
$ kill -CONT <PID>
```

```
$ kill -KILL <PID>
```
 
### Exercici 3

Posa exemples utilitzant fg, bg, &, <Ctrl + z>, <Ctrl + c> i explica que fan.

### Exercici 4

Obriu dos emuladors de terminals (o dues consoles virtuals). Des d'una terminal engegeu un procés llarg com per exemple:

```
ls -lRtshi /
```

Des de l'altre terminal heu de:

1. Parar o *fer una pausa* del procés (alerta, no volem terminar-lo)

2. Continuar el procés anterior.

### Exercici 5

Com a root, xequejem si tenim engegat el procés (o servei o dimoni) postgresql:

```
[root@pc95 ~]# systemctl status postgresql.service
```

Si està parat, engeguem-lo:

```
[root@pc95 ~]# systemctl start postgresql.service
```

Suposem ara que acabem de fer algun canvi en un fitxer de configuració i volem que el servei postgresql se n'adoni.

Amb quina ordre i senyal farem això que volem?

A on es troba el *log* que ens informa que efectivament s'han tornat a llegir els fitxers de configuració de `postgresql`?

### Exercici 6

Estem utilitzant firefox i s'ha penjat. Volem enviar-li una senyal perquè acabi (de bon rotllo). Fes-ho de 3 maneres:

* Trobant prèviament el PID i utilitzant pipes (grep,cut ...)

* Sense necessitat de saber el seu PID, només el nom del proces i sense pipes.

* Trobant prèviament el *PID* (hint: `pgrep`)

Per treure nota: la resposta que probablement donareu a la primer opció fa que el propi proces de busqueda es mostri.
Com ho podríem fer perque no es mostrés?

### Exercici 7

Volem executar una ordre o script en 2on pla des de la terminal, tancar la terminal i que el procés continui executant-se.

* Quina altre ordre podem anteposar (a la nostra ordre o script) per aconseguir això?
* Per què l'ordre anterior funciona amb sintaxi:

	```
	ordre bash script.sh &
	```

	o

	```
	ordre sh script.sh &
	```

	però no amb:

	```
	ordre . script.sh &
	```

	o

	```
	ordre source script.sh &
	```

* EXTRA BONUS: Suposem que no he executat la misteriosa ordre anterior i tinc
  un procés associat a la meva terminal executant-se en 2on pla, puc aconseguir
que el meu procés es "desvinculi" de la terminal?

### Exercici 8

Un alumne a classe executa des d'una terminal l'ordre gedit de la següent manera:
```
gedit &
```

vol comprovar que si fa exit des de la consola (igual que tancant la terminal amb el ratolí) el procés gedit s'acaba. Però s'emporta la sorpresa de que no funciona així, ja que en realitat al fer exit es desvincula el procés de la terminal, de la mateixa manera que fa l'ordre de la'apartat c) anterior.

Per il·lustrar una mica aquest fet observeu des d'una terminal amb l'ordre:
```
watch  'pstree -sp $(pgrep gedit)'
``` 
I des d'una altra terminal executeu:

```
gedit &
```

Detecteu el procés _gedit_ a l'arbre de `pstree` i després feu:

```
exit
```

Torneu a buscar el procés _gedit_ a l'arbre de `pstree` i com els seus pares (processos) originals han canviat.

* Que fa l'ordre _watch_?

* Que fan les opcions _-s_ i _-p_ de _pstree_?

* Són necessàries les cometes simples sempre o en aquest cas?

### Exercici 9

Completa el següent script:

```
#!/bin/bash
# Fitxer: 		scriptVIP.sh
# Autor: 		jamoros
# Data: 		23/01/2014
# Versio: 		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
# 				See http://www.gnu.org/licenses/gpl.html for more information."
# Descripcio:	Script que realitza una operació tan important que no permet que
# 				ni el senyal 2)SIGINT que s'aconsegueix amb Ctrl+C ni 15)SIGTERM parin el procès.
# 				Ho fem utilitzant la comanda "trap"

# Ordre que fa que quan aquest script s'estigui executant (i per tant sigui un
# procès) i rebi un dels 2 senyals abans esmentats, no realitzi l'acció associada
# a aquest senyal i en el seu lloc executi l'acció/ordre que nosaltres volguem,
# com en aquest cas mostrant un missatge.

# ?????

# Bucle que mostra cada 10 segons un missatge
# (un xic exagerat, sobre la importància de la feina que fa el propi script)
while true
do
	sleep 10
	echo "Estic fent un treball crític"
done
```

