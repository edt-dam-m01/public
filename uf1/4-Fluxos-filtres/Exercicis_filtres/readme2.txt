The following is a listing of the differences between IBM's mappings
of certain IBM code pages and Unicode, and the mappings for IBM code
pages that came from various sources and were earlier made available
at the Unicode ftp site.

The first set of differences refers to tables found originally in the
Microsoft directory at unicode.org.  The DBCS tables were in the
EastAsiaMaps directory. The findings are based on the content of the
files as of April 26, 1996. Newer versions may result in fewer
differences.

IBM has many conversion tables to Unicode available in a binary (machine
readable) format on a CD-ROM included with the document SC09-2190-00 'CDRA
Reference and Registry'.  IBM has also defined new code pages which contain
the Euro Sign. For further information, please contact the IBM National
Language Technical Centre (nltc@ca.ibm.com).

=======================================================================
Code Pages 00437 (US etc.)
           00860 (Portugal)
           00861 (Iceland)
           00862 (Israel)
           00863 (Canadian French)
           00865 (Nordic)

      Microsoft                         IBM
      ---------                       -----
0x1A      U001A                       U001C
0x1C      U001C                       U007F
0x7F      U007F                       U001A
0xE6      U00B5 (MICRO SIGN)          U03BC (GREEK SMALL LETTER MU)

The "rotation" of the control characters at 0x1A, 0x1C and
0x7F is due to the frequent use of 0x1A as end-of-file by
PC file systems and applications.

=======================================================================
Code Page 00852

          Microsoft                         IBM
          ---------                       -----
0x1A      U001A                       U001C
0x1C      U001C                       U007F
0x7F      U007F                       U001A
0xAA      U00AC (NOT SIGN)             ----  (Unassigned)

Note that Microsoft code page 852 has a "not sign" in position 0xAA
whereas IBM 852 has 0xAA as unassigned.  In IBM mappings, unassigned
code points in a code page are generally mapped to SUB (U001A) in the
binary tables.  The readable tables do not contain entries for these
points.

