# Fluxos de text i filtres

_T'has llegit el text d'IBM 103.2 Text streams and filters? Sí? Llavors pots començar:_

Fitxers auxiliars: - [fitxersDeText.txt](fitxersDeText.txt) - [readme1.txt](readme1.txt) - [readme2.txt](readme2.txt) - [oficinas.dat](oficinas.dat) - [repventas.dat](repventas.dat) - [file1](file1) - [file2](file2) - [file3](file3)

1. Donats els dos fitxers "repventas.dat" i "oficinas.dat", vull saber quin és exactament el caràcter delimitador de camps utilitzat a aquests fitxers.

    - Amb un editor com _cat_, _less_ o _more_ no puc diferenciar uns espais en blanc d'un tabulador. Però al tema que estem treballant actualment hi ha una ordre que em permet veure de quins caràcters exactament està compost un fitxer. En particular podré saber quin és el caràcter delimitador de camps (“columnes”).

    - Troba l'ordre i escriu l'ordre i el resultat que demostra quin és el delimitador per a “repventas.dat”. Anàlogament per a l'altre fitxer.

2. Estudiem l'ordre _cut_. Aquesta ens mostra seccions de cada línia dels fitxers. En particular, si un fitxer està ben ordenat, amb camps (columnes) i delimitadors (un caràcter que fa de separador de camps), podem fer que ens mostri les columnes que vulguem d'un cert fitxer.

    - Quin és el paràmetre que em serveix per indicar el caràcter delimitador? Si no poso res quin és el caràcter que agafa per defecte com a delimitador (en anglès **delimiter**) ? Quin és el paràmetre que indica per quina columna o camp (en anglès **field**) tallem el text o sigui el mostrem?

    - Mostra el camp 1 del fitxer "oficinas.dat".

    - Mostra el camp 1 i el 2.

    - Mostra el camp 1 i el 3, 4 i 5

    - Suposa que no saps quants camps hi ha, mostra tots els camps menys el 2.

3. Amb quina ordre podria saber les oficines que tinc ? He de suposar que per cada línia, o sigui per cada **\n**, hi haurà una oficina.

4. Volem canviar el delimitador que hi ha a "oficinas.dat" pel caràcter ":", com ho faries ?

    - I fer aquest canvi i a més que el nou fitxer tingui com a nom "oficinas.txt" ? Resoleu aquest exercici amb 2 ordres diferents. (Feu el mateix amb "repventas.dat")

5. Mostreu ara els camps 1 i 3 del nou fitxer oficinas.txt

6. Mostreu les possibles regions que hi pot tenir una oficina. Òbviament no hi ha d'haver duplicats.

    - Si busqueu al _man_ adient trobareu almenys dues possibles maneres de fer això. (Si no ordeneu primer, no us anirà bé)

7. Ordena "repventas.txt" pel 3er camp. Un cop trobis l'ordre adient, fixa't que necessites passar-li alguna cosa més que el camp pel qual vols ordenar.

8. Feu el mateix per ordenar "repventas.dat" pel 3er camp. (Aquesta és moooolt difícil)

9. Ara que ja heu aprés a ordenar fitxers, ordeneu "repventas.txt" pel 4t camp (no de forma numèrica, que si no la lia amb el NULL) i deseu-lo com a "repventasOrd.txt". Feu el mateix amb "oficinas.txt" pel primer camp i deseu-lo a "oficinasOrd.txt".

10. Ara que teniu 2 fitxers que tenen 2 camps en comú i que a més estan ordenats, podem fer servir el _join_ per unir els 2 fitxers pel camp que tenen en comú. Per a a això hem d'indicar el delimitador i els camps de cada fitxer que volem unir.

11. Suposeu que teniu algun problema amb el reconeixement del vostre pen USB quan el connecteu amb el vostre Fedora 20. En anteriors versions de del sistema operatiu es podia llegir el fitxer `/var/log/messages`, que era el principal fitxer de servei-dimoni de logs, _syslogd_. L'ordre per llegir «en directe» era:   `tail -f /var/log/messages`. Ara el dimoni que es fa servir és _journald_. I amb l'ordre adequada es poden llegir els missatges del sistema.

    Digues quina és l'ordre que:

    - permet llegir els missatges del sistema
    - permet llegir en directe els missatges del sistema
    - permet llegir els missatges d'arrencada (boot)

    (Per fer la 2a ordre executa la mateixa ordre a la consola i després connecta el pen USB, observa els missatges que surten)

12. Amb l'ordre _split_ algú ha dividit un fitxer en 3 parts (`file1`, `file2` i `file3`), però sembla que després s'han canviat els noms dels fitxers, de manera que no estan ordenats. Heu de descobrir, a pèl, quin és l'ordre en el qual s'han d'ajuntar aquests trossos de fitxers perquè puguem obtenir el fitxer original. Amb quina ordre obtenim, a partir d'aquests 3 fitxers, el fitxer original?

    Un cop descobriu això, torneu a partir el fitxer però ara volem que els 3 fitxers resultants siguin de 10000000 de bytes (aproximadament 10 MB) i que els noms siguin `file00`, `file01`, `file02`. Amb quina ordre ho aconseguim?
