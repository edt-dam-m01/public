#### Mínim exigit per treballar amb l'editor VIM:

* Diferents maneres de passar de mode ordre a mode inserció.
* El mateix per a l'operació inversa.
* Com es desa amb el mateix nom o diferent?
* Com es desa i es surt?
* Com es surt desfent els canvis?
* Si tinc un fitxer al meu directori que és de només lectura existeix una combinació de tecles 
que ens permet saltar-nos aquesta limitació?
* Maneres de desfer les modificacions fetes (undo) i torna-les a fer (redo).
* Com fer cerques (com repetir la darrera cerca).
* Com fer substitucions (a una línia, a un conjunt de línies, a tot el document)
* Com seleccionar un bloc de text (*marcar*).
* Com copiar un bloc de text.
* Com tallar un bloc de text.
* Com enganxar (paste) un bloc de text.
* Com passar de majúscules a minúscules tot un bloc de text.
* Com passar de minúscules a majúscules tot un bloc de text.
* Com jugar amb els diferents registres que ens proporciona el vim editor per poder copiar i 
pegar diferents blocs de text.
* Com posar diferents opcions a l'editor (longitud dels tabuladors, identació automàtica, 
numeració de línies...).
* Fitxer de configuració.
* Fitxer de backup.
