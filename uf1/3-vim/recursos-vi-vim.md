## Recursos vi/vim

- [Una xuleta](vimqrc.pdf)
- [Un emulador: Vimulator](http://thoughtbot.github.io/vimulator/)
- [Un joc: VIM Adventures](https://vim-adventures.com/)
- [Un tutorial interactiu: Open Vim](http://www.openvim.com/)

