# **Llapis USB amb partició ext4**

## Objectiu
L'objectiu d'aquest document és aconseguir un llapis USB amb dos particions:

- Una amb format NTFS o FAT32 per a poder-se utilitzar en ordinadors amb diferents sistemes operatius (GNU/Linux, Windows, Apple).
- Una altra amb format ext4 per a poder compartir fitxers entre ordinadors amb GNU/Linux o diferents usuaris d'un mateix ordinador.

Tenim dos problemes:

- El Windows només reconeixerà la partició número 1 del llapis; per tant, aquesta serà la que tindrà el sistema de fitxers NTFS o FAT32
- El sistema de fitxers ext4 assigna a cada fitxr l'UID de l'usuari que l'ha creat i, normalment, no permet que el modifiqui un altre usuari. Segurament, el nostre usuari de l'institut i el de casa tindran diferent UID. L?objectiu principal d'aquest document és explicar com aconseguim que en un sistema de fitxers ext4 qualsevol usuari pugui llegir i modificar qualsevol fitxer o directori del sistema, independentment de quin hagi sigut l'usuari que l'ha creat.

## Problema dels permisos
Si desconeixeu el problema dels permisos, no feu el pas "Llista de control d'accés (ACL)" i comproveu-lo. Després fareu aquest pas per a solucionar-lo.

## Esbrinem quin és el dispositiu del nostre llapis USB
```
lsblk
```

A partir d'aquest moment, suposarem que és `/dev/sdx`.

## El particionem
```
# fdisk /dev/sdx
```

Creem les particions `/dev/sdx1` de tipus `NTFS` i `/dev/sdx2` de tipus `ext4`.

## Formatem les dues particions

Podeu posar les etiquetes que us vinguin més de gust.

```
# mkfs.ntfs -L ETIQUETA-NTFS /dev/sdx1
# mkfs.ext4 -L ETIQUETA-EXT4 /dev/sdx2
```

## Llista de control d'accés (ACL)

Aquest mètode ens permetrà modificar els permisos que s'estableixen per defecte en els fitxers i directoris que es creïn en aquesta partició.

Establim ACL en aquesta partició:
```
tune2fs -o acl /dev/sdx2
```

La muntem, canviem els permisos i després modifiquem l'ACL per tal d'aconseguir el que volem.
```
# mount /dev/sdx2 /mnt
# chmod 777 /mnt
# setfacl -m d:u::rwx,d:g::rwx,d:o::rwx /mnt
```

A partir d'aquest moment, en el moment de crear-los s'establiran uns permisos 666 a tots els fitxers i 777 a tots els directoris.

Font: [https://unix.stackexchange.com/questions/422656/how-to-make-an-ext4-formatted-usb-drive-with-full-rw-permissions-for-any-linux-m](https://unix.stackexchange.com/questions/422656/how-to-make-an-ext4-formatted-usb-drive-with-full-rw-permissions-for-any-linux-m)
