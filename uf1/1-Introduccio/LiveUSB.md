# **Live USB (senzill i multiboot)**

En GNU/Linux tenim diferents formes d'aconseguir un llapis USB arrencable, amb una distro Live per a poder provar-la o instal·lar-la.

## Un programa ad hoc
Hi ha diferents programes especialment dissenyats per a això. Alguns d'ells permeten conservar el contingut que tingués el llapis USB però d'altres l'eliminen. Heu d'anar amb atenció amb això.

- `Fedora Media Writer`, que és el que proposa Fedora en la seva web
- `UNetbootin`
- ...

## Utilitat `dd`

Aquesta opció eliminarà tot el que hi ha al llapis USB i a més a més no permetrà després escriure-hi (el farà només de lectura), fins que no el tornem a formatar.

Amb `lsblk` esbrinem quin és el dispositiu del nostre llapis USB. Suposem que és `/dev/sdx`

```
dd if=fitxer.iso of=/dev/sdx
```

Si us fa problemes, afegiu l'opció `bs=4M`. Quedaria:

```
dd if=fitxer.iso of=/dev/sdx bs=4M
```

## Multiboot USB

Aquest programa ens permet de tenir en un mateix llapis USB diferents sistemes operatius Live; per exemple, Fedora 27, Fedora 24, Ubuntu 18.10. Fins i tot, algunes versions de Windows, com ara Gandalf.

Aquest programa és lliure (GPL), però no està dsponible en els dipòsits de Fedora. Això, però, no és un gran problema, ja que la instal·lació és molt fàcil.

Descarregueu-lo de la seva web [http://multibootusb.org](http://multibootusb.org) i instal·leu-lo ambl'ordre
```
# dnf -y install multibootusb-9.2.0-1.noarch.rpm
```

En Fedora, aquesta ordre instal·larà les dependències necessàries. En altres SO, com Debian i Ubuntu, segurament necessitareu instal·lar primer les dependències. Llegiu la guia que hi ha en la seva web.

L'ús del programa és molt intuïtiu i, a més a més, està molt ben documentat en la web.

En el llapis USB farà dos directoris (`EFI` i `multibootusb`) i conservarà tot el contingut que tinguéssiu en el llapis, però un mínim de prudència ens demana que fem una còpia de seguretat abans, naturalment.

