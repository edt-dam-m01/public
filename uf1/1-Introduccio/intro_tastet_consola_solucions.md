# **_Solucions dels exercicis d'introducció a la línia d'ordres_**


1. A quin directori us trobeu?

	Amb l'ordre **pwd** (_print working directory_) ho veurem:
	```
	pwd
	```
	Si us trobeu al directori *practica1*, després d'executar **pwd** la sortida serà semblant a:
	```
	/home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1
	```
	Tingueu en compte que xxxxx són els números del vostre NIF i que ja existeixen els directoris necessaris: *M01\_Sistemes_Informatics*

2. Quines són les ordres que heu emprat per crear aquesta estructura de directoris?

	Es pot fer de diferents maneres, veiem-ne unes quantes.
	Suposem que em trobo a **~/iamxxxxxx/M01_Sistemes_Informatics/practica1**.

	Si utilitzo rutes relatives podria fer:
	```
	mkdir subdirectori1
	mkdir subdirectori1/subdirectori11
	mkdir subdirectori1/subdirectori12
	mkdir subdirectori2
	mkdir subdirectori2/subdirectori21
	mkdir subdirectori2/subdirectori22
	mkdir subdirectori2/subdirectori22/subdirectori221
	mkdir subdirectori2/subdirectori22/subdirectori222
	```
	Si, en canvi, utilitzo trajectòries absolutes, tant és que el directori actiu (on em trobo) sigui *practica1*, com */tmp*, com qualsevol altre. Les instruccions sempre seran:
	```
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori1
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori1/subdirectori11
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori1/subdirectori12
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori2
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori2/subdirectori21
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori2/subdirectori22
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori2/subdirectori22/subdirectori221
	mkdir /home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori2/subdirectori22/subdirectori222
	```
	Òbviament podem estalviar uns quants caràcters reescrivint les ordres anteriors gràcies a la variable `HOME` o al caràcter especial `~`:
	```
	mkdir HOME/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori1
	```
	o:
	```
	mkdir ~/iamxxxxxx/M01_Sistemes_Informatics/practica1/subdirectori1
	```

3. Hi ha algun paràmetre que permeti crear més d'un directori de cop?

	La pregunta que ens podem fer ara és *És necessari que existeixi prèviament un directori per poder crear un subdirectori dintre seu?* La resposta es troba al man de l'ordre *mkdir*.
	Hem de trobar amb quin paràmetre es pot crear més d'un directori de cop ? És a dir, hem de cercar quina opció em permet crear un subdirectori i els subdirectoris pares (o mares i avis o àvies  ...) necessaris si no existeixen.
	La solució és el paràmetre *-p* (en anglès *parent*).
	Mirem una possible solució utilitzant rutes relatives si em trobo al directori */home/users/inf/hiam1/iamxxxxxxx/iamxxxxxx/M01_Sistemes_Informatics/practica1*:
	```
	mkdir -p subdirectori1/subdirectori11
	mkdir -p subdirectori1/subdirectori12      # aquí ja no cal -p
	mkdir -p subdirectori2/subdirectori21
	mkdir -p subdirectori2/subdirectori22/subdirectori221
	mkdir -p subdirectori2/subdirectori22/subdirectori222  #aquí tampoc
	```
	Però també és possible relacionar més d'un directori dintre de les claus **{** **}** separades per comes **SENSE** espai darrera de les comes. En efecte:
	```
	mkdir -p subdirectori1/{subdirectori11,subdirectori12}
	mkdir -p subdirectori2/{subdirectori21,subdirectori22}
	mkdir  subdirectori2/subdirectori22/{subdirectori221,subdirectori222}
	```
	I *el més difícil encara*, en una única ordre:
	```
	mkdir -p {subdirectori1/{subdirectori11,subdirectori12},subdirectori2/{subdirectori21,subdirectori22/{subdirectori221,subdirectori222}}}
	```
4. Col·loqueu-vos al vostre home, editeu amb vi un fitxer anonomenat *fitxerA* dintre del directori *subdirectori222* i deseu-lo.
	```
	cd  #ara ens trobem al nostre HOME: /home/users/inf/hiam1/iamxxxxxxx
	```

	```
	vim M01_Sistemes_Informatics/practica1/subdirectori2/subdirectori22/subdirectori222/fitxerA
	```
	I un cop dintre de vim, per canviar de mode ordre a mode inserció premem la tecla `i` i posteriorment escrivim qualsevol cosa.
	Finalment, canvien a mode ordre prement la tecla `Esc` i escrivim l'ordre que desa i surt del fitxer al mateix temps `:wq`  (*write* i *quit*)

5. Copieu-lo a “subdirectori12” (indiqueu on es trobeu)

	Em trobo a HOME,  ho veig fent:
	```
	pwd
	```
	Utilitzo rutes relatives:
	```
	cp M01_Sistemes_Informatics/practica1/subdirectori2/subdirectori22/subdirectori222/fitxerA    M01_Sistemes_Informatics/practica1/subdirectori1/subdirectori12
	```
	Suposem que em trobés a subdirectori222, llavors l'ordre seria:
	```
	cp fitxerA  ../../../subdirectori1/subdirectori12
	```

6. Canvieu el nom del *fitxerA* que es troba a *subdirectori222* per *fitxerB*

	Si em trobo a subdirectori222
	```
	mv fitxerA fitxerB
	```
7. Elimineu el subdirectori222. Que passa? No feu res, però quina seria la solució?
	```
	rm subdirectori222
	```
	No em deixa perquè diu que és un directori. Amb l'ordre rmdir tampoc perquè no està buit: hi ha un fitxer.
	Si al man busquem la paraula clau *directory* trobem la solució:
	```
	rm -r subdirectori222
	```

8. Moveu el fitxer *fitxerB* que es troba a *subdirectori222* al *subdirectori12*.

	Suposem que em trobo a _subdirectori222_, llavors l'ordre és:
	```
	mv fitxerB ../../../subdirectori1/subdirectori12
	```
