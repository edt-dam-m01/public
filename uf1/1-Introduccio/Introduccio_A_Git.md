# Programari per al control de versions: GIT

## Introducció

Anem a treballar amb un programari de control de versions *cvs* (_concurrent version systems_ o _sistema de control de versions_). 

Aquest sistema ens permet tenir un registre de tot el treball realitzat i els canvis fets, de manera que facilita la recuperació de versions antigues d'aquest treball.
Encara té una utilitat més important que és la de facilitar el treball col·laboratiu, per exemple un equip de desenvolupadors,  utilitzant el concepte de branques, aquesta però és una part que no veurem en aquesta introducció.

D'altra banda no hem de pensar en aquest software com una eina exclusiva de desenvolupadors, ja que la tipologia d'usuaris de git inclou fins i tot [escriptors](https://www.penflip.com/).

## Definició
GIT és un sistema de versions distribuït, per tant podem tenir rèpliques del nostre treball en diferents ordinadors.

Quan fem un canvi en el nostre ordinador el podem distribuir a tantes màquines com vulguem per tal de tenir una còpia dels canvis.
També podem baixar-nos una còpia d'aquests canvis des d'on hi hagi alguna rèplica allà on vulguem.

Normalment, treballarem amb l' ordinador de l'escola, el Fedora del meu portàtil, el Debian del mateix portàtil, l'Ubuntu d'una torre que tinc ... 
i mantindrem una còpia del nostre treball en algun dels diferents hostings que ens ofereix el mercat. 
Aquests hostings, a més de guardar una altra còpia del nostre treball, ens ofereixen una interfície web que ens permet veure els canvis fets,
l'històric i molt més d'una manera amigable. 
Si bé és cert que si algú té algun ordinador que ofereix servei a internet 24  hores i vol instal·lar-se alguna d'aquestes aplicacions web per *git* ho pot fer (*Gitlab* és una opció).

El més popular és *Github*, ofereix repositoris gratuïts però tots públics, i si volem un privat és de pagament. 
Tot i tenir una gran quantitat de projectes de programari lliure, paradoxalment *Github* no ho és.
D'altra banda tenim *GitLab*, que sí que és programari lliure i que ens ofereix el mateix que Github i a més repositoris privats de manera gratuïta. 
Hi ha altres serveis que sí que són de pagament.

## Transferència de dades mitjançant ssh. La clau ssh.

Hi ha diferents mètodes per transferir les dades entre diferents ordinadorsperò en aquesta introducció, parlarem només de ssh i https. Tots dos són segurs, però https no pot automatitzar la comunicació amb el servidor i demanarà la contrasenya (de GitLab) cada vegada. Per a això ens quedem amb el mètode per ssh. Costa una mica de configurar, però després només ens demanarà una sola vegada per sessió, la contrasenya que hagem posat en crear les claus, que pot ser buida sense cap roblema.

Escollim el mètode ssh i per utilitzar-lo necessitem tenir una clau ssh
que ens permet establir una connexió segura entre el nostre ordinador i l'ordinador on desem la còpia,
en aquest cas *GitLab* 

Des del nostre ordinador creem una clau publica i una privada que fan parella.
Si li passem a un altre ordinador la nostra clau publica, al fer la connexió entre els dos ordinadors,
es comprova que les dues claus són parella, en cas afirmatiu hi haurà confiança,
i això permetrà la connexió segura.

Per tant, el nostre objectiu es crear aquesta parella de claus al nostre pc i pujar la clau pública a *Gitlab*. Això ho farem una mica més avant, seguins les indicacions d'aquest mateix document.

## Instal·lació del programari *git*

Per treballar amb git necessitem instal·lar el paquet git, per tant com a root:
```
dnf install git
```

## Creació d'un compte a *Gitlab*

Anem a la pàgina de [gitlab](https://gitlab.com) i ens registrem. També podeu donar-vos d'alta en [github](https://github.com) i comparar-los.

## Descripció del funcionament bàsic

Ara mostrarem una forma de funcionament molt bàsic, per tal d'introduir-se en el tema. Naturalment, `man git` us resoldrà tots els dubtes que tingueu.

## Creem un projecte nou (buit) en la web de GitLab.com

Escollim un nom per al projecte, el nom del repositori git del projecte 
no pot contenir espais en blanc, ja que serà una URI.
El nom del projecte, que es pot modificar en qualsevol moment, sí que pot tenir espais en blanc. 

## En cada ordinador on anem a treballar:

Els punts 1, 2 i 3 només cal fer-los una vegada en cada ordinador. El punt 4 s'ha de fer cada vegada que hi treballem.

1. Creem un parell de claus (pública i privada). Instruccions en GitLab.com → Clic a la icona del "profile" a dalt a la dreta → Settings → SSH Keys → generate one

   ```
   ssh-keygen -t rsa -C "your.email@example.com" -b 4096
   ```
   - Canviem your.email@example.com pel correu que vam utilitzar per a crear el compte en GitLab.com
   - Respectem (no canviem) el fitxer de destinació id_rsa
   - Podem deixar la contrasenya buida
2. Enganxem la clau pública en GitLab.com
   - Copiem el contingut del fitxer ~/.ssh/id_rsa.pub
   - L'enganxem en GitLab.com → Clic a la icona del "profile" a dalt a la dreta → Settings → SSH Keys → Key
   - Posem com a títol alguna cosa que ens identifiqui l'ordinador. Per exemple, Ordinador de l'aula, Portàtil de casa...
   - Add key
3. Preparem per a treballar en l'ordinador. Instruccions en GitLab.com → Clic al nom del projecte
   - Git global setup
      ```
      git config --global user.name "El vostre nom"
      git config --global user.email "el vostre correu electrònic"
      ```
   - Copiem (clonem) el projecte. En aquest cas, el projecte encara està buit, però quan ja contingui fitxers, es farà igual.
      ```
      git clone git@gitlab.com:nomdusuari/nomdelprojecte.git
      ```
   - Entrem en el directori del projecte. A partir d'ara, treballarem sempre des d'aquest directori
      ```
      cd nomdelprojecte
      ```
4. Passos a seguir en cada sessió de treball
   - Baixar-nos les novetats que hi pugui haver, del treball fet en altres ordinadors (des de dins del directori del projecte)
      ```
      git pull
      ```
   - Treballem amb els fitxers i directoris del projecte
   - Comprovem els canvis que hi ha per a guardar (afegir) en el projecte
      ```
      git status
      ```
   - Afegir al projecte tots els canvis que hem fet
      ```
      git add .
      ```
   - Fer un commit (una instància/versió, que podrem revisar en el futur)
      ```
      git commit -m "Nom del commit"
      ```
   - Pujar els canvis al servidor
      ```
      git push
      ```

## Exercici 1

Practiqueu ara una mica fent el següent. Creareu el directori *dir1* dintre de *my-first-project* (o el nom que li hagueu posat).
Dintre de *dir1* creareu un fitxer de nom *fitxer.txt* i amb el vim afegireu 3 línies.
Aquesta serà un primera versió del document, després eliminareu la darrera línia del fitxer i aquesta serà també una altra versió del document.
Un cop s'hagin pujat els canvis a l'ordinador remot de gitlab comproveu que no només podeu veure la darrera versió sinó també l'anterior.

## Exercici 2

Anem a simular en aquest exercici que estem treballant amb dos ordinadors i a més volem tenir còpia a Gitlab mitjançant ssh.
És molt important tenir present que no estem fent treball col·laboratiu o en equip, per tant no es farà cap merge (treballarem amb una única branca: la del master)

La manera de fer la simulació serà la següent:
* Treballarem amb dos pc's: *pc\_escola* i *pc\_casa*, però no al mateix temps.
* Pensarem que un pc és el de l'escola i l'altre és el de casa. O sigui farem veure que hem arribat a casa molt ràpid.
* Hi ha d'haver 2 claus ssh (una per cada pc: «pc_escola» i «pc_casa») i totes dues pujades a Gitlab.
* No es poden fer les operacions de pujada (push) o baixada (pull) de forma des-sincronitzada: heu de simular que sou un únic usuari i per tant no podeu fer 2 coses al mateix temps.

Les operacions que fareu seran:
* Fer la configuració adient de les claus ssh.
* Crear un repositori a Gitlab per exemple git-repository-exercici.
* Al *pc\_escola* cloneu el projecte de Gitlab . Afegiu un o dos programes senzills a dins (tipus *Hello World*)
* Ara haureu de jugar amb *add* per afegir els diferents fitxers a *git* que heu modificat i *commit* amb un missatge descriptiu,
per tenir una nova versió del directori. Podeu fer tants add's i commit's com considereu necessaris. I quan penseu que és hora de «plegar» feu un push per sincronitzar la informació amb l'ordinador de Gitlab.
* Aneu a l'altre pc, *pc\_casa*, baixeu-vos el projecte.
* Afegiu un altre fitxer amb un programa senzill.
* Feu les proves que considereu convenients usant els tres ordinadors (els dos de l'aula i el de Gitlab).


## Esquema projecte GIT

Un projecte GIT el podem pensar de la següent manera:

![Imatge on surt l'esquema dels tres elements més importants en un projecte GIT](Introduccio_A_Git.png)


* Un *directori de treball*: a on farem tot el treball editant, creant, esborrant i organitzant els fitxers.
* Una *àrea temporal*:  a on s'inclouran els canvis fets al directori de treball
* Un *repositori*: a on *Git* emmagatzemarà permanentment els canvis anteriors com a versions diferents del mateix projecte



## Extra links: 
* [The simple guide\. no deep shit](http://rogerdudler.github.io/git-guide/)
* [Manual de git fet per un professor del nostre departament](https://Gitlab.com/jordinas/gitet)
* [SSH error: Permission denied](https://help.github.com/articles/error-agent-admitted-failure-to-sign)

---

