### Instal·lació i ús del programa dia

Per a instal·lar-lo, com sempre:

```
# dnf install dia
```

El hub (concentrador), el switch (commutador) i el router (encaminador) els trobareu en el full "Cisco - switch" i trobreu imatges d'ordinadors en "Cisco - Computer".

