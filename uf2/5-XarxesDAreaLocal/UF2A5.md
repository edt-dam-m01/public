### Les xarxes d'àrea local

1. Amb el programa DIA, fes un esquema de la xarxa que tinguis a casa, a la feina o a l'escola. Quina o quines topologies de xarxa observes?

2. Poseu-vos en grups i simuleu una xarxa amb topologia d'anell i pas de testimoni com a control d'accés al medi. Mireu aquet vídeo: [https://www.youtube.com/watch?v=50RUTSbTSR8](https://www.youtube.com/watch?v=50RUTSbTSR8)

3. Quin és el funcionament del control d'accés al medi CSMA/CD?

4. A l'aula quins dispositius de xarxa pots enumerar? Es compleixen els requisits del cablatge estructurat?

5. Digues quines diferències hi ha entre un switch i un router.

6. De cadascuna de les següents xarxes, contesta:

   - Quants dominis de col·lisió hi ha? Descriu cadascun d'aquests dominis dient quins són els trams de xarxa que els formen.

   - Quants dominis de broadcast hi ha? Descriu cadascun d'aquests dominis dient
quins són els trams de xarxa que els formen.

      a.

![UF2A5-a.jpg](UF2A5-a.jpg)

      b.

![UF2A5-b.jpg](UF2A5-b.jpg)

