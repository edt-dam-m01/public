### Activació/desactivació peticions icmp broadcast

`icmp_echo_ignore_broadcasts`

Turn on (1) or off (0), if the kernel should ignore all ICMP ECHO and TIMESTAMP requests to broadcast and multicast addresses. Off by default.

Please note that if you accept ICMP echo requests with a broadcast/multicast destination address **your network may be used as an exploder for denial of service packet flooding attacks to other hosts**.

For accepting ICMP echo requests with a broadcast/multicast destination address:

`echo "0" >  /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts`

o equivalentment:

`sysctl -w net.ipv4.icmp_echo_ignore_all=0`

Les ordres anteriors són temporals, per tant quan tornem a engegar la màquina el valor tornarà al default.

Si volguessim que el canvi fos definitiu (en principi no recomanable) hauríem d'afegir al fitxer `/etc/sysctl.conf`:

`net.ipv4.icmp_echo_ignore_all = 1`

i perquè el canvi tingui efecte immediat:

`# sysctl -p`

For more information, see `man icmp`
