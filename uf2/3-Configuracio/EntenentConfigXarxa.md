### Entenent config. xarxa (IP, default GW, DNS server)

Per entendre la configuració de xarxa, desfem i refem la configuració pas a pas: 
- Treure config DNS eliminarà l'acces per nom a qualsevol host
- Treure default GW eliminarà l'accés a hosts d'altres xarxes
- Treure IP eliminarà l'access a hosts de la mateixa xarxa

I després refer pas a pas, comprovant a cada pas què és el que torna a funcionar.

Passos:

1.1) Comprovem la nostra configuració actual<br>
`ip address show` -> la nostra IP i la màscara (xarxa, broadcast address). Ex. `192.168.0.17/16`<br>
`ip route list` -> la ruta per defecte, i la ruta per la nostra xarxa. Ex. `192.168.0.1`<br>
`cat /etc/resolv.conf` -> el servidor DNS `192.168.0.10`

1.2) A partir del nom, obtenim la IP  d'un host a la mateixa xarxa i d'un host extern<br>
`host gandhi` -> Té la IP `192.168.0.10`   (És el mateix que tenim com DNS server!)<br>
`host www.xtec.cat` -> Té la IP 83.247.151.214<br>
`host moodle.escoladeltreball.org` -> Té la IP 10.1.81.195   (En realitat es interna, accedim sense internet, però en una altra subnet de l'escola del treball. La IP externa és 85.192.81.130. Podeu comprovar-ho des de fora de l'Escola del Treball)

1.3) Comprovem que tenim accés per nom i per IP tant a la pròpia xarxa, com a l'altra subxarxa, com a internet<br>
IP host mateixa xarxa -> `ping -c1 192.168.0.10`<br>
nom host mateixa xarxa -> `ping -c1 gandhi`<br>
<span style="margin-left:12em">`host gandhi`</span><br>
IP host extern -> `ping -c1 83.247.151.214`<br>
<span style="margin-left:7.5em">`host 83.247.151.214`</span><br>
<span style="margin-left:7.5em">`wget http://83.247.151.214`</span><br>
nom host extern-> `ping -c1 www.xtec.cat`<br>
<span style="margin-left:8.1em">`host www.xtec.cat`</span><br>
<span style="margin-left:8.1em">`wget http://www.xtec.cat`</span><br>

2.1) Treiem la config. del DNS server (Cal copiar el fitxer!!).<br>
Abans de modificar fitxers de configuració SEMPRE s'ha de fer còpia; hem de poder restaurar la configuració.<br>
`mv /etc/resolv.conf /etc/resolv.conf.orig`

2.2) Comprovem que podem accedir per IP a qualsevol host, però no per nom

2.3) En /etc/resolv.conf posem DNS server 8.8.8.8<br>
`nameserver 8.8.8.8`<br>
Fem ping per nom i veurem que torna a funcionar...<br>
Observeu que , com que el DNS 8.8.8.8 és extern, per a moodle.escoladeltreball.org ens dóna l'adreça externa.<br>
Esborrar de nou el fitxer; quedarem sense DNS.

Nota: Com que és extern, si posem aquest servidor de DNS quan no tenim defaultt GW, no serà accesible.

El que tenim normalment com està a la mateixa xarxa sí és accesible quan no tenim el default GW

3.1) Esborrem ruta per defecte<br>
`ip route del default via 192.168.0.1 dev enp2s0`<br>
Comprovem com queda:<br>
`ip route list`

3.2) Comprovem que podem accedir només als hosts de la mateixa xarxa

4.1) Esborrem la IP<br>
`ip address del 192.168.0.17/16 dev enp2s0`

4.2) Comprovem que no podem accedir a cap node. (només al propi: 127.0.0.1, pel dev lo)

5) Tornem a recuperar-ho tot: primer IP, despres la ruta i despres el DNS<br>
Comprovem a cada pas què funciona i què no
