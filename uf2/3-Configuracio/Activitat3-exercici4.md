### Activitat 3. Exercicici 4.

    4. Configureu una petita xarxa local (temporal), una per fila (2/3 ordinadors) seguint els següents passos (enumereu l'ordre o ordres que feu servir per a cada pas):
        a. Afegiu una IP a cadascuna de les màquines.
        b. Comproveu la connexió amb el programa ping.

**PAS PREVI. BACKUP CONFIG. ACTUAL**

- Guardeu copia de la configuració permanent actual. No la tocarem, farem tot amb ordres que modifiquen la configuració temporal. Però per si de cas:<br>
`cp /etc/sysconfig/network-scripts/ifcfg-enp2s0 ~/`

**PAS1. ESBORRAR DADES XARXA ACTUAL**

- Esborreu la ip actual<br>
`ip address del <ip actual> dev enp2s0`

   També es pot fer amb ‘flush’ que allibera tota la configuració per la interfície<br>
`ip address flush dev enp2s0`

- Comproveu amb `ip address show` que no teniu IP

- comproveu amb `ip route list` que no teniu rutes definides 

**PAS2. DEFINIR IP EN LA XARXA QUE CORRESPON PER TAULA**

- Numerem les taules. Per cada taula considerarem la xarxa:<br>
   `172.16.<numTaula>.0/24`<br>
   O sigui, tindrem les xarxes:<br>
   taula 1 - xarxa `172.16.1.0/24`<br>
   taula 2 - xarxa `172.16.2.0/24`<br>
   taula 3 - xarxa `172.16.3.0/24`<br>

- A cada ordinardor afegiu la IP, segons la taula i el número d’ordinador:<br>
   `172.16.<numTaula>.<numOrdinador>/24`

   `ip address add 172.16.<numTaula>.<numOrdinador>/24 dev enp2s0`

   per exemple: per la taula 1, ordinador J02:
   `ip address add 172.16.1.100/24 dev enp2s0`

- Comproveu amb `ip address show`  que la  IP està bé

- comproveu amb `ip route list` que automàticament ha afegit la ruta per la nostra xarxa `172.16.<numTaula>.0/24`

- Comproveu que funciona un ping a vosaltres mateixos<br>
   `ping 172.16.<numTaula>.<numOrdinador>`

   així comproveu que no us heu equivocat en escriure la IP.

- Comproveu que funciona ping a un altre ordinador de la nostra taula (mateixa xarxa)<br>
   `ping 172.16.<numTaula>.<altreNumOrdinador>`

- Comproveu que **no** funciona ping a un ordinador d'una altra taula (xarxa diferent)<br>
   `ping 172.16.<altreNumTaula>.<altreNumOrdinador>`

   En aquest cas l’error indica _Network is unreachable_.

- Comproveu que el missatge d’error és diferent, quan **no** funciona ping a la mateixa xarxa perquè l'ordinador no existeix<br> 
   `ping 172.16.<numTaula>.<altreNumOrdinadorNoExisteixi>`

   En aquest cas l’error indica _No existeix el HOST_


**PAS3. ENRUTAR CAP ALTRA XARXA**

Farem que tots els ordinadors d’una xarxa vegin els d’una altra xarxa:

- Afegiu ruta d'una taula a la següent<br>
   per exemple de taula 1 a taula 2.<br>
   En els ordinadors de la taula 1:<br>
   `ip route add 172.16.2.0/24 dev enp2s0`

   Els ordinadors de la taula 1 poden enrutar cap els ordinadors de la taula 2. Però el ping complet segueix sense anar: el missage de sortida anirà, però de la taula 2 no sap enrutar a la taula 1 per contestar)

- Comproveu amb `ip route list` que teniu dues rutes: la ruta per a la nostra xarxa (nostra taula), i per a la següent 

- Afegiu ruta d'una taula a l'anterior<br>
   per exemple de taula 2 a taula 1.<br>
   En els ordinadors de la taula 2:<br>
   `ip route add 172.16.1.0/24 dev enp2s0`

- Comproveu amb `ip route list` que teniu 3 rutes: ruta per la nostra xarxa (nostra taula), per a la següent i per a l'anterior

- Comproveu que funciona un ping a un ordinador de la següent taula


**PAS4. RECUPERAR LA CONFIGURACIO INICIAL**

- Comproveu amb la còpia, que la configuració permanent no s’ha modificat:<br>
   `diff /etc/sysconfig/network-scripts/ifcfg-enp2s0 ~/ifcfg-enp2s0`<br>
   Si heu treballat sempre amb ordre `ip`, no estarà modificat.<br>
   Es modifica des de l’eina gràfica, o amb l’ordre `nmcli`

- Elimineu la configuració actual:<br>
   `ip address flush dev enp2s0`

- Actualitzeu la configuració amb DHCP:<br>
   `dhclient enp2s0`

-  Si no funciona bé, allibereu primer el que teníem de DHCP executant-se anteriorment:<br>
   (amb `ps -fea | grep dhclient` podem veure que s’estava executant el process `dhclient`)<br>
   `dhclient -r`<br>
   `killall dhclient`<br>
   `dhclient enp2s0`

