### Configuració de la xarxa amb GNU/Linux

##### Servei de xarxa

+ Mostrem estat interfícies

	```
	ip link show  # mostra informació de totes les interfícies
	```

	o

	```
	ip l show enp5s0   # mostra informació de la interfície enp5s0
	```

	*l=link*

+ Activem/desactivem interfície

	```
	ip link set [interficie] up|down
	```

+ Activem/desactivem configuracions de xarxa (amb els paràmetres dels fitxers de configuració)

	```
	ifup|ifdown [interficie]
	```

+ Iniciem/parem/reiniciem el servei de xarxa (amb els paràmetres dels fitxers de configuració)

	```
	systemctl  status|start|stop|restart NetworkManager.service
	```

	*Abans hi havia algunes diferències entre Debian i Fedora (`service`, `/etc/init.d/network-manager` ...)*

	Si en comptes del servei de xarxa *Network-Manager* s'utilitzés *network* hauríem de canviar el nom del servei.

+ Establim/Traiem servei de xarxa permanentment:

	+ systemctl  enable|disable NetworkManager.service	
	+ Creant/Esborrant enllaços simbòlics manualment
	+ OLD: system-config-services (gràfic, Fedora, s'ha d'instal·lar)
	+ OLD: setup (a les darreres versions s'ha d'instal·lar system-config-network)
	+ OLD: chkconfig --level [runlevel] network on|off

		Una manera ràpida de veure si NetworkManager ja està “enabled” és amb l'ordre:
	
		```
		systemctl is-enabled NetworkManager.service; echo $? 
		```
	
	+ OLD Debian: sysv-rc-conf
	+ OLD Debian: update-rc.d networking defaults (activar)
	+ OLD Debian: update-rc.d -f networking remove	(desactivar)

##### Informació de les interfícies

+ Mostrem informació de totes les interfícies

	```
	ip addr show 
	```

+ Mostrem informació d'una interfície concreta

	```
	ip addr show dev [interficie]
	```

##### Configuració temporal

+ Manual

	+ Afegim IP
	
		```
		ip addr add [adr_ip] broadcast [adr_broadcast] dev [interficie]
		```

	+ Treure IP

		```
		ip addr del [adr_ip] dev [interficie]
		```

		L'adreça ip tindrà el format `ip/numero_bits_reservats_per_identificar_la_xarxa`

	+ Veure taula d'enrutaments

		```
		ip route
		```
	
		ro=route

	+ Afegir un enrutament¹

		```
		ip route add [adr_xarxa] via [adr_gw] dev [interficie]
		```

	+ Esborrem enrutament

		```
		ip route del [adr/xarxa]
		```

	+ Afegir gateway

		```
		ip route add default via [adr_gw] dev [interficie]
		```

	+ Esborrem gateway

		```
		ip route del default via [adr_gw] dev [interficie]
		```

+ Per DHCP

	```
	dhclient [interficie]
	```

##### Configuració temporal xarxes sense fils

Tots els paràmetres es configuren igual excepte els paràmetres específics de wireless que ho farem amb la següent comanda:

```
iwconfig [interficie] essid [essid] mode [mode] key s:[clau]
```

Per a una connexió des de client el [mode] serà managed

#### Configuració permanent

+ Programa gràfic escriptori Gnome:

	```
	gnome-control-center network
	```

	![gnome-control-center_network.png](gnome-control-center_network.png)

+ Consola: modificant els fitxers de configuració de la xarxa

	+ Fedora
		`/etc/sysconfig/network-scripts/ifcfg-interface` a on interface és el nom del dispositiu de xarxa
		per exemple i aquest dispositiu fos `em1` el fitxer seria `/etc/sysconfig/network-scripts/ifcfg-em1`
		
	+ Debian
		`/etc/networking/interfaces`

+ Altres: `nmcli` (Network Manager consola), `nmtui` (Network Manager ncurses).

##### Configuració DNS

+ Els mateixos que a l'apartat anterior (`system-config-network`, `nmcli`, `nmtui`)
+ Consola: modificant els fitxer de configuració de resolució de [dominis de] noms: `/etc/resolv.conf`


##### Nou estàndard per nombrar els dispositius de xarxa

Ja fa uns anys que Fedora (F15) va adoptar un nou estàndard per als dispositius de xarxa. El que abans es deia eth0, eth1 ... a Fedora ja fa temps que té un nom en funció de si és una targeta "built-in" (a la placa mare), una PCI, etc.

S'intenta que els noms no depenguin de quina interfície s'ha activat abans sinó que siguin uns noms fixos i únics. És el que se'n diu *Consistent Network Device Naming*. Es pot trobar més informació a:

+ [Wikipedia](https://en.wikipedia.org/wiki/Consistent_Network_Device_Naming)
+ [RedHat 7](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/ch-consistent_network_device_naming#sec-Naming_Schemes_Hierarchy)
+ [Understanding systemd’s predictable network device names](https://major.io/2015/08/21/understanding-systemds-predictable-network-device-names/)


¹ Podem afegir una ruta estàtica a un únic host o a tota una xarxa, per tant alerta, hem de tenir clem que no és el mateix 192.168.1.1 ([adr_host]) que 192.168.1.0/16 ([adr_xarxa])

_No utilitzem *ifconfig* ja que el mateix `man` diu que el programa és obsolet._
