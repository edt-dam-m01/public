```
CFGS DAM - M01
UF2: Gestió de la informació i recursos de xarxa 
Activitat 0
```

## Intro

1. Llegiu la Declaració Independència del ciberespai: 
   - [ca] [http://dmi.uib.es/~valverde/declara.html](http://dmi.uib.es/~valverde/declara.html)
   - [es] [http://biblioweb.sindominio.net/telematica/manif_barlow.html](http://biblioweb.sindominio.net/telematica/manif_barlow.html) 
   - [en] [http://homes.eff.org/~barlow/Declaration-Final.html](http://homes.eff.org/~barlow/Declaration-Final.html)

i anoteu les conclusions a les quals arribeu dels següents punts:

    a) Reclamacions que fa la declaració. 
    b) Quines són les mesures de control que existeixen a Internet? Avantatges? Inconvenients? 
    c) La mercantilització d'Internet: Avantatges? Inconvenients?

2. Llegiu els següents dos articles que parlen de com comportar-se a Internet: 
   - La netiqueta: [http://www.netiqueta.org/](http://www.netiqueta.org/) 
   - Com fer preguntes de manera intel·ligent: [https://wiki.edubuntu.org/CatalanTeam/Recursos/ComPreguntar](https://wiki.edubuntu.org/CatalanTeam/Recursos/ComPreguntar)

Imagineu-vos la següent situació:
 
Acabes de començar la UF2 de Fonaments de Xarxes. En la primera classe de la UF de xarxes a un vídeo surt un router i un switch. No tens clar que fa un router, ni un switch i menys encara la diferència entre els dos, però et fa vergonya preguntar-ho perquè sembla que tothom ho hagi entès... Com que et quedes amb el neguit decideixes enviar un correu a una llista especialitzada per resoldre els teus dubtes. 

    a) Envia dos correus al fòrum de l'assignatura amb el dubte exposat. Un correu ha de complir la netiqueta i l'altre no. 
    b) Llegeix els correus que han enviat els teus companys i escull aquell que creguis que és més correcte i el que menys.
