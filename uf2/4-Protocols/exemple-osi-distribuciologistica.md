### Exemple model OSI - DISTRIBUCIÓ LOGÍSTICA

Una cooperativa agrícola ven productes en un mercat a una ciutat d'un altre país.
El procés de recollida de la fruita a la cooperativa i l'enviament al mercat ho podem desglossar per NIVELLS OSI:

L7 - APLICACIÓ - recollida fruita, negociació del preu de venda, decideix enviament

L6 - PRESENTACIÓ - empaquetament en cistelles amb un pes determinat i dins de caixes precintades

aspecte de la fruita + compressió + precintat (encriptació)

L5 - SESSIÓ - Acordat l'enviament serà de 10 tones cada dilluns, dimecres i divendres. Però la pròxima setmana, divendres festiu, passa a dijous.
I pagament a 30 dies

L4 - TRANSPORT - és dilluns, Trucar companyia de transport que reculli la fruita i que entregui al mercat en el termini fixat
(termini d'entrega)

L3 - XARXA - La companyia de transport determina les rutes, el trasllat de càrrega: 5 tones en avió i 5 tones en camió. La càrrega per avió en un contenidor especial, i per camió en caixes de cartó.

L2 - ENLLAÇ - el contenidor de l'avió pesa massa, es reparteix en dos, i es situen en una banda i altra de l'avió. Camions frigorífics: repartit en 10 camions, cada càrrega precintada i cada camió precintat. Si temperatura NOK es descarta la càrrega i es demana reposició
(control d'errors)

L1 - FISIC - avió transporta fins destí. Camions arribar a destí

