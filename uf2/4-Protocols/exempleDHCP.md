## Pràctica a classe

# Exemple funcionament capes de protocols amb el protocol DHCP.


Capturarem amb el wireshark missatges del protocol DHCP per entendre millor els nivells i entitats al model TCP/IP (o UDP/IP en aquest cas) i al model de referència OSI

El protocol DHCP (Dynamic Host Configuration Protocol) s’utilitza per assignar de forma dinàmica adreces IP als nodes de la xarxa. És cada node qui sol·licita al servidor que li assigni una adreça IP.
 

El funcionament en el nostre exemple: 

 - en un servidor de la xarxa de l'escola, ja s’està executant la aplicació **servidor DHCP**. És una aplicació que està permanentment escoltant pel port 67 UDP, esperant a  rebre peticions dels clients. 
 - en el nostre ordinador, farem que l’aplicació client DHCP sol·liciti la configuració de xarxa a aquest servidor DHCP.
 - El servidor DHCP assigna:
	- adreça IP dinàmica
	- màscara de xarxa
	- ruta (o gateway)


Els missatges que s’intercanvien l’aplicació **client DHCP** i l’aplicació **servidor DHCP**, són els missatges definits en el protocol  DCHP:

- DISCOVERY - Missatge que envia el client de DHCP, ho envia a tots els nodes de la xarxa perquè no sap qui és el node que està executant el servidor DHCP.

- OFFER - Missatge de resposta del servidor DHCP,  proporciona dades de configuració de xarxa 

- REQUEST - Missatge que envia el client DHCP al servidor DHCP per indicar que accepta les dades de configuració proporcionades al missatge OFFER. 

- ACKNOWLEDGE - Missatge de confirmació que envia el servidor DHCP al client.


De forma més gràfica:
client DHCP    ----------- DISCOVERY --------------->   servidor DHCP (port 67 UDP)
 
client DHCP    <------------- OFFER -------------------   servidor DHCP
(port 68 UDP)
 
client DHCP    -------------- REQUEST --------------->   servidor DHCP

client DHCP     <----------- ACKNOWLEDGE -------   servidor DHCP




El protocol DHCP és un protocol de capa de aplicació, és el que parlen i entenen **l’aplicació client DHCP** que tenim en el nostre ordinador i **l’aplicació servidor DHCP** que està en un servidor de la xarxa.

Veurem amb el wireshark, les capes que intervenen en aquesta comunicació. I el protocol a cada capa:

- protocol **DHCP** - a la capa d'aplicació 
- protocol **UDP**  - a la capa de transport   (port UDP)
- protocol **IP**  - a la capa de xarxa (adreça IP)
- protocol **Ethernet**  - a la capa d'enllaç (adreça MAC)

Un **port UDP** identifica el socket per intercanviar informació entre la capa de transport (UPD) i la capa d'aplicació. Els ports UDP utilitzats pel protocol DHCP són:
- port UDP 67 - servidor DHCP
- port UDP 68 - client DHCP

A la capa de xarxa,  **l'adreça IP** identifica un node dins d’un xarxa. És una adreça lògica, que es pot modificar.

A la capa d'enllaç, **l'adreça MAC** identifica un dispositiu de xarxa. L' adreça MAC és una adreça física que vé ja assignada pel fabricant a cada dispositiu.



###Les ordres a Linux que necessitem conèixer de moment:

1. **ip address show**
Per veure la configuració de xarxa actual. Podem tenir vàrios enllaços (o interfícies) de xarxa, i per cada enllaç mostra les dades de configuració: adreça IP, màscara de xarxa,..


2. **dhclient enp2s0**
Renovar l’adreça IP de l'enllaç enp2s0 (el nom de l'enllaç o interfície ho canviarem d'acord amb la informació obtinguda abans). Per defecte treballa amb protocol DHCPv4 per obtenir una adreça IPv4. 
Amb l’opció -v (verbose), veurem informació dels paquets intercanviats amb el servidor DHCP:
**dhclient -v enp2s0**

3. **dhclient -r**
“Release” configuració DHCP


###Capturar els paquets DHCP:
Al nostre ordinador tenim una aplicació que és un client DHCP (/usr/bin/dhclient). L'executarem al mateix temps que capturarem el tràfic amb el wireshark.
I amb els paquets capturats amb el wireshark analitzarem millor les capes i com funciona l’intercanvi de missatges.

Prèviament hem d'instal·lar el wireshark (dnf install wireshark-gtk)



	1)  Per capturar els paquets DHCP amb el wireshark:
Capture / Options... 

	- seleccionar la interfície de xarxa (enp2s0 en el nostre cas), 
	- omplir el filtre de captura. Per capturar els paquets del protocol DHCP, el que farem es capturar els paquets del protocol UDP que continguin el port 67 o el port 68:
	udp port 67 or port 68
	- Start, per començar la captura

(si no posem filtre de captura, es capturen tots els paquets que pasen per la interfície de xarxa seleccionada)

      2) en un terminal executeu el client DHCP (sol·licita les dades al servidor DHCP i configura la xarxa al nostre ordinador)
**dhclient -v enp2s0**

	3)  atura la captura del wireshark (amb el recuadre vermell de dalt)





##Analitzem com funciona l'enviament del missatges DHCP i el contingut dels paquets que hem capturat:


###missatge DISCOVERY DHCP (client -> servidor)

A) En el nostre ordinador, quan l'aplicació client DHCP ha enviat el missatge DISCOVERY:

- l'aplicació client DHCP és l'entitat de la **capa d'aplicació**. Aquesta entitat ha passat les dades DHCP (el missatge DISCOVERY) al servei de la capa de transport (nivell 4 OSI).  També obre un socket al port 68 UDP, i es queda escoltant d'aquest port per rebre la resposta.

- la **capa de transport** (protocol UDP), ha afegit una capçalera, i ha passat les dades al servei de la capa de xarxa (nivell 3 OSI): 
**dades capa de transport (UDP) = capçalera transport + dades capa aplicació (missatge DISCOVERY DHCP)**
Així l'entitat del nivell 3 (capa de transport) en el destí que entèn el protocol UDP, amb la capçalera afegida, sabrà interpretar la informació.

- la **capa de xarxa** (protocol IP), ha afegit una capçalera, i passa les dades al servei de la capa d'enllaç (nivell 2 OSI): 
	**dades capa xarxa (IP) = capçalera capa xarxa + dades capa de transport (UDP)**
	
- la **capa d'enllaç** (protocol Ethernet), ha afegit una capçalera, i bytes de padding al final, i passa les dades al servei de la capa física (nivell 1 OSI):
 	**trama = capçalera capa d'enllaç (Ethernet) + dades capa xarxa (IP) + final trama**
- la **capa física** retransmet la trama als altres ordinadors de la xarxa.


B) Al wireshark, podem veure el paquet complet com està viatjant per la xarxa.

Permet veure la informació dels protocols per cada capa (nivell ISO):

- frame.  Tots els bits del paquet que viatjen per la xarxa
- Ethernet. A la capçalera podem veure:
	- adreça MAC origen: la MAC del nostre ordinador
	- adreça MAC destí: una MAC de broadcast, per enviar a tots els nodes de la xarxa
- IP. A la capçalera podem veure:
	- adreça IP origen: la IP del nostre ordinador
	- adreça IP destí: una IP de broadcast, per enviar a tots els nodes de la xarxa
- UDP. A la capçalera podem veure:
	- port UDP origen: 68 (client DHCP)
	- port UDP destí: 67 (servidor DHCP)


C) En el servidor quan es rep el paquet, per fer que arribi a l'aplicació **servidor DHCP**:
- la **capa física** passa la trama a la capa d'enllaç.
- la **capa d'enllaç** (Ethernet), rep la trama, comprova amb la adreça MAC a la capçalera ethernet si és el destinatari. Si ho és, extreu la capçalera ethernet, i els bits de padding al final, i passa les dades a la capa de xarxa.
 	**dades capa xarxa (IP) = capçalera capa xarxa + dades capa de transport (UDP)**
 
- la **capa de xarxa** (IP), comprova amb l'adreça IP a la capçalera si és el destinatari. Si ho és, passa les dades a la capa de transport 
	**dades capa de transport (UDP) = capçalera transport + dades capa aplicació (missatge DISCOVERY DHCP)**

- la **capa de transport** (UDP), comprova el port UDP destinatari, i li envia les dades a l'aplicació corresponent, l'aplicació que està escoltant per aquest port.
	**dades capa aplicació (missatge DISCOVERY DHCP)**
- l'aplicació rep les dades que contenen el missatge DISCOVERY enviat


D) En altres nodes de xarxa que reben el paquet i no hi ha l'aplicació **servidor DHCP**:

- la capa d'enllaç (Ethernet), rep la trama, comprova amb la adreça MAC a la capçalera ethernet si és el destinatari. Si ho és, extreu la capçalera ethernet, i els bits de padding al final, i passa les dades a la capa de xarxa.
 	**dades capa xarxa (IP) = capçalera capa xarxa + dades capa de transport (UDP)**
 
- la capa de xarxa (IP), comprova amb l'adreça IP a la capçalera si és el destinatari. Si ho és, passa les dades a la capa de transport 
**dades capa de transport (UDP) = capçalera transport + dades capa aplicació **

- la capa de transport (UDP), comprova el port UDP destinatari, com no hi ninguna aplicació en aquest port, descarta el missatge.





###missatge OFFER DHCP (servidor -> client)

A) En el servidor, quan l'aplicació servidor DHCP envia el missatge OFFER:

- la capa d'aplicació ha passat les dades DHCP (missatge OFFER) a la capa de transport. 
- la capa de transport (UDP), ha afegit una capçalera, i passa les dades a la capa de xarxa: 
	**dades capa transport(UDP) = capçalera capa transport + dades capa d'aplicació**
Així la capa UDP en el destí amb la capçalera afegida sabrà interpretar la informació
- la capa de xarxa (IP), ha afegit una capçalera, i passa les dades a la capa d'enllaç: 
	**dades capa xarxa (IP) = capçalera capa xarxa + dades capa transport**
- la capa d'enllaç (Ethernet), ha afegit una capçalera, i bytes de padding al final, i passa les dades a la capa física:
 	**trama = capçalera Ethernet + dades capa xarxa + final trama**
- la capa física retransmet la trama als altres ordinadors de la xarxa.


B) Al wireshark, podem veure el paquet complet com viatjarà per la xarxa.

Permet veure la informació dels protocols per cada capa:

- frame.  Tots els bits del paquet que viatjen per la xarxa
- Ethernet. A la capçalera podem veure:
	- adreça MAC origen: la MAC del servidor DHCP
	- adreça MAC destí: la MAC del nostre ordinador
- IP. A la capçalera podem veure:
	- adreça IP origen: la IP del servidor DHCP
	- adreça IP destí: la IP que el servidor DHCP ens proporciona per al nostre ordinador
- UDP. A la capçalera podem veure:
	- port UDP origen: 67 (servidor DHCP)
	- port UDP destí: 68 (client DHCP)


C) En el nostre ordinador quan es rep el paquet, per fer que arribi a l'aplicació **client DHCP**:

- la capa d'enllaç (Ethernet), rep la trama, comprova amb la adreça MAC a la capçalera ethernet si és el destinatari. Si ho és, extreu la capçalera ethernet, i els bits de padding al final, i passa les dades a la capa de xarxa.
 	**dades capa xarxa (IP) = capçalera capa xarxa + dades capa transport**
 
- la capa de xarxa (IP), comprova amb l'adreça IP a la capçalera si és el destinatari, (aquest és un cas especial, al nostre ordinador encara no tenim IP). Si ho és, passa les dades a la capa de transport 
	**dades capa transport(UDP) = capçalera capa transport + dades capa d'aplicació**

- la capa de transport (UDP), comprova el port UDP destinatari, i li envia les dades a l'aplicació corresponent, l'aplicació que està escoltant per aquest port.
	**dades capa aplicació (missatge OFFER DHCP)**
- la aplicació rep les dades que contenen el missatge OFFER enviat


D) En altres nodes de xarxa que reben el paquet i no sigui el nostre ordinador:

- la capa d'enllaç (Ethernet), rep la trama, comprova amb la adreça MAC a la capçalera ethernet que no és el destinatari, i descarta el paquet.




