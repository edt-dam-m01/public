### packet tracer 7.0 per fedora 24

Packet tracer 7.0 ja va amb linux:

Descarregar des de: [https://drive.google.com/a/correu.escoladeltreball.org/file/d/0B05N4JmI5cVfSnFjMlczcXRYaTg/view?usp=sharing](https://drive.google.com/a/correu.escoladeltreball.org/file/d/0B05N4JmI5cVfSnFjMlczcXRYaTg/view?usp=sharing)

```
su -
dnf install zlib-devel ncurses-devel gtk2 glibc glibc-devel  libstdc++ libX11-devel libXrender libXrandr libusb libXtst nss  qt qtwebkit
wget  http://bt0.ninja/rpm/openssl-lib-compat-1.0.0i-1.fc24.x86_64.rpm
rpm -Uvh openssl-lib-compat-1.0.0i-1.fc24.x86_64.rpm
mkdir /opt/pt

tar xvfz cisco.tar.gz
cd PacketTracer70/
./install
# al procés de instal·lació escollir directori /opt/pt

chown -R your_username /opt/pt
exit

#com usuari
/opt/pt/packettracer &
```

