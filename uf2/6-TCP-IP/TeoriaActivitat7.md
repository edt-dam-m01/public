## Adreça IP
L'adreça IP són 32 bits. Es representa amb 4 números decimals separats per punts, que van de 0 a 255. Cada número decimal correspon a 8 dígits binaris.

Exemple: `212.13.14.122 = 11010100.00001101.00001110.01111010`

## Tipus de xarxes

#### Classe A
- format: 0nnnnnnn.hhhhhhhh.hhhhhhhh.hhhhhhhh
- 1er numero decimal: 0 (00000000) - 127 (01111111)
- número de xarxes: 2^7 = 128
- número de hosts per xarxa: 2^24 - 2 = 16777214

#### Classe B
- format: 10nnnnnn.nnnnnnnn.hhhhhhhh.hhhhhhhh
- 1er numero decimal: 128 ( 10000000) - 191 (10111111)
- número de xarxes: 2^14 = 16384
- número de hosts per xarxa: 2^16 - 2 = 65534

#### Classe C
- format: 110nnnnn.nnnnnnnn.nnnnnnnn.hhhhhhhh
- 1er numero decimal: 192 (11000000) - 223 (11011111)
- número de xarxes: 2^21 = 2097152
- número de hosts per xarxa: 2^8 - 2 = 254

#### Classe D
- format: 1110xxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx 
- 1er numero decimal: 224 (11100000) - 239 (11101111)
- direccions multicast

#### Classe E
- format: 1111xxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx
- 1er numero decimal: 240 (11110000) - 255 (11111111)
- direccions reservades

| clas- se | format | rang | Màscara | CIDR | número de xarxes | número de hosts per xarxa (\*) |
|:------:|:------:|:----:|:-------:|:----:|:----------------:|:------------------------------:|
| A | 0nnnnnnn.hhhhhhhh.hhhhhhhh.hhhhhhhh | 0.0.0.0 - 127.255.255.255 | 255.0.0.0 | /8 | 128 | 16.777.214 |
| B | 10nnnnnn.nnnnnnnn.hhhhhhhh.hhhhhhhh | 128.0.0.0 - 191.255.255.255 | 255.255.0.0 | /16 | 16.384 | 65.534 |
| C | 110nnnnn.nnnnnnnn.nnnnnnnn.hhhhhhhh | 192.0.0.0 - 223.255.255.255 | 255.255.255.0 | /24 | 2.097.152 | 254 |
| D | 1110xxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx | 224.0.0.0 - 239.255.255.255 |               |     |           |     |
| E | 1111xxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx | 240.0.0.0 - 255.255.255.255 |               |     |           |     |

(\*) S'estan descomptant l'adreça de xarxa i l'adreça de broadcast

## Adreces de propòsit especial

Hi ha diferents adreces reservades. Aquí s'exposen les més importants. Per saber-les totes es pot consultar el [RFC 3330](http://tools.ietf.org/html/rfc3330).

#### Adreces especials
- direccions de xarxa: bits del host a 0
- direccions de broadcast: bits del host a 1
- 255.255.255.255: broadcast
- 0.0.0.0: mateix ordinador que l'envia
- 127.x.x.x: loopback

#### Adreces reservades per a adreces privades
- 10.\*.\*.\*
- 172.16.\*.\* - 172.31.\*.\* (10101100.00010000.\*.\* - 10101100.00011111.\*.\*)
- 192.168.0.\* - 192.168.255.\* (11000000.10101000.00000000.\* - 11000000.10101000.11111111.\*)

## Glossari

**IP pública**: ip visible des d'Internet

**IP privada**: ip no visible des d'Internet i visible des de la nostra LAN

**IP fixa (o estàtica)**: ip fixada, no canvia

**IP dinàmica**: ip que s'obté a partir del servei DHCP i que, per tant, pot variar.

Més informació: [https://en.wikipedia.org/wiki/IP_address](https://en.wikipedia.org/wiki/IP_address)

