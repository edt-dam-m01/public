### Protocol IEEE 802.3 (Ethernet)

Preàmbul - SFD - capçalera IEEE 802.3 - Dades ::: - FCS

- **Preàmbul**: 7 bytes sempre iguals. S'usa per a estabilitzar el medi físic. 

- **SFD, Start Frame Delimiter**: 8 bits. Sempre té el valor 0xAB. Indica que el preàmbul acaba i que ara sí que la trama comença.

- **Capçalera IEEE 802.3**: Formada per tres camps
    - **Adreça Destí**: 6 bytes. Adreça MAC del node destí. Pot ser una adreça unicast, multicast o broadcast.
    - **Adreça Origen**: 6 bytes. Adreça MAC unicast MAC del node origen.
    - **Tipus**: 16 bits. Tipus de protocol de la capa superior.

- **Dades**: Mida variable, de 46 a 1500 bytes.

- **FCS, Frame Check Sequence**: 4 bytes. CRC usat per verificar la integritat de la trama.

**Mida total de la trama**: de 64 a 1.518 bytes (no es té en compte el preàmbul i el SFD).

Referència: [http://www.networksorcery.com/enp/protocol/IEEE8023.htm](http://www.networksorcery.com/enp/protocol/IEEE8023.htm)

### Adreça MAC

![Protocol Ethernet](ProtocolEthernet.png)

### Exemples de filtres amb Wireshark

Trames Ethernet:

`eth`

Trames que tinguin com a origen o bé la MAC AA:AA:AA:BB:BB:BB o bé la MAC CC:CC:CC:DD:DD:DD:

`eth.src == AA:AA:AA:BB:BB:BB || eth.src == CC:CC:CC:DD:DD:DD`

Trames que tinguin com a origen la MAC AA:AA:AA:BB:BB:BB i com a MAC destí CC:CC:CC:DD:DD:DD:

`eth.src == AA:AA:AA:BB:BB:BB && eth.dst == CC:CC:CC:DD:DD:DD`

Trames que tinguin com a origen la MAC AA:AA:AA:BB:BB:BB i com a MAC destí CC:CC:CC:DD:DD:DD o trames que no continguin paquets IP:

`(eth.src == AA:AA:AA:BB:BB:BB && eth.dst == CC:CC:CC:DD:DD:DD) || eth.type != 0x800`

Trames que tinguin per destí l'adreça MAC broadcast i continguin un paquet IP:

`eth.dst == FF:FF:FF:FF:FF:FF && eth.type == 0x800`

Trames que tinguin per destí una adreça MAC multicast o broadcast i continguin un paquet IP:

`eth.ig == 1 && eth.type == 0x800`

