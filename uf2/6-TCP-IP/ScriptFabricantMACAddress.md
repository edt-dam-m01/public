### Script per obtenir el fabricant de la Mac Address (opcional)

Volem fer un script per obtenir el fabricant d'una Mac Address qualsevol.

Feu un script que comprovi que disposem de connexió a Internet i en cas afirmatiu que ens mostri per pantalla una capsa del zenity on introduirem una adreça MAC.

L'script fara un control d'errors, de manera que entrem una adreça real MAC, recordeu que estem parlant d'adreces de la forma

XX:XX:XX:XX:XX:XX   on X és un dels digits {0,1,2....9,A,B,C,D,E,F} (majúscules o minúscules)

Un cop validem l'adreça explotarem alguna de les API que es troben a:

- [http://www.macvendorlookup.com/mac-address-api/](http://www.macvendorlookup.com/mac-address-api/)
- [https://api.macvendors.com/](https://api.macvendors.com/)
- [https://macvendors.co/api/](https://macvendors.co/api/)

o altres que trobareu a Internet

per tal de mostrar les dades del fabricant ("vendor" en anglès), de fet es podria obtenir molta més informació.

Habitualment, funcionen posant la MAC Adress després de la URL, però haureu d'investigar una mica.

Tags: ping, curl (o wget), expressions regulars...


