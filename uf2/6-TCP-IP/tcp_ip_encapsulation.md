### Encapsulació TCP/IP

![Encapsulació TCP/IP](tcp_ip_encapsulation.gif)

Inicialment realitzem alguna acció al nivell superior amb una aplicació (espai d'usuari), des de cercar una web amb un navegador fins a fer un ping amb la terminal.

Cada capa del model TCP/IP afegeix informació (capçalera=header) a les dades de l'aplicació.

Finalment arribem a la trama Ethernet (ethernet frame) que conté tota la informació que viatjarà per la xarxa.

