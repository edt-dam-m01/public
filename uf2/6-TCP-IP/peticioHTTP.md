### Petició HTTP: analitzar capçaleres protocols amb wireshark

Veure petició HTTP amb wireshark



[http://xtec.gencat.cat/ca/](http://xtec.gencat.cat/ca/)


Obtenir:

- de la capçalera Ethernet:
    - MAC origen
    - MAC destí
- de la capaçalera IP:
    - IP origen
    - IP destí
- de la capçalera TCP:
    - TCP port origen
    - TCP port destí
- de la capçalera HTTP:
    - HTTP mètode (GET, POST,...

Intentar trobar el misstge de resposta a la petició. I obtenir
- de la capçalera HTTP
    - codi resposta (200-OK, 301 o 302 - redireccio,..)
