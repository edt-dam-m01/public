### Protocol IP

Capçalera MAC - Capçalera IP - Dades :::

- **Preàmbul**: 7 bytes sempre iguals. S'usa per a estabilitzar el medi físic. 

- **SFD, Start Frame Delimiter**: 8 bits. Sempre té el valor 0xAB. Indica que el preàmbul acaba i que ara sí que la trama comença.

### Capçalera IP

- **Versió**: Versió del protocol. Amb IPv4: 0100 (4bits).

- **IHL, Longitud de la capçalera**: Longitud en paraules de 4B. La capçalera haurà de ser múltiple de 4B i tindrà com a mínim 20B i com a màxim 60B (4bits).

- **TOS, Tipus de servei**: Paràmetres que ens indicaran si el paquet ha de tenir un tracte especial.

- **Longitud Total**: Longitud total del paquet en bytes (2bytes). La longitud del paquet serà sempre menor a 65.535B

- **Identificació**: Identificador únic del paquet

- **Indicadors**: Està composat per 3 bits:

    - bit R (reservat): 0

    - bit DF (don't fragment): 0 (el paquet es pot fragmentar, si cal), 1 (el paquet no es pot fragmentar)

    - bit MF (more fragments): 0 (aquest és l'últim fragment d'un paquet), 1(hi ha més fragments després d'aquest)

- **Posició del fragment**: Posició del fragment (per poder reconstruir el paquet).

- **TTL, temps de vida**: màxim número de routers que pot travessar el paquet

- **Protocol**: Protocol que conté el paquet. Els més importants són TCP (6), UDP(17), ICMP(1)

- **Suma de verificació de la capçalera**: Control d'errors de la capçalera.

- **Adreça IP origen**: IP del host que ha generat el paquet

- **Adreça IP destí**: IP del host que ha de rebre el paquet

- **Opcions**: Diferents serveis.

- **Farciment**: Farciment per garantir que la capçalera arriba a la seva longitud mínima (32B)

Referència (consulteu-la per a més detalls): [http://www.networksorcery.com/enp/protocol/ip.htm](http://www.networksorcery.com/enp/protocol/ip.htm)

### Exemples de filtres amb Wireshark

Paquets IP:

`ip`

Trames que tinguin com a origen o bé la IP 192.168.0.12 o bé la IP 192.168.0.169 i com a destí la IP 192.168.0.1:

`(ip.src == 192.168.0.12 || ip.src == 192.168.0.169) && ip.dst == 192.168.0.1`

Trames Ethernet que tinguin com a origen la MAC AA:AA:AA:BB:BB:BB i com a MAC destí CC:CC:CC:DD:DD:DD o trames de qualsevol tipus que no continguin paquets IP:

`(eth.src == AA:AA:AA:BB:BB:BB && eth.dst == CC:CC:CC:DD:DD:DD) || !ip`

Trames Ethernet amb paquets IP que no siguin TCP:

`eth.type == 0x800  && ip.proto != 6`

Paquets ip que la seva mida sigui superior a 1KBla seva longitud sigui més gran a 1 KB, on la seva capçalera medeixi 20B:

`ip.len > 1024 && ip.hdr_len == 20`

### Fragmentació

La MTU (Maximum Transfer Unit) ens defineix la longitud màxima de dades que es poden transmetre dins d'una trama en un tipus de xarxa concret. A Ethernet la MTU és de 1500B.

Per tant, si tenim un paquet IP que la seva longitud supera la MTU, hem de fragmentar aquest paquet dividint-lo en paquets més petits. Ens ajudarem del bit MF i del camp de posició del fragment de la capçalera IP, per tal que el host destí pugui reconstruir la informació rebuda.

Es pot evitar la fragmentació usant la tècnica del Path MTU discovery, que consisteix en saber quin és el mínim MTU que ens trobarem en el camí que ha d'empendre la nostra trama. Un cop sabem aquest MTU mínim només construirem trames d'aquest tamany. Per saber com funciona aquesta tècnica cliqueu el següent enllaç: [PMTU(Path MTU) Discovery](https://web.archive.org/web/20160310165234/http://www.netheaven.com/pmtu.html).
