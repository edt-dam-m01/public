<!DOCTYPE html>
<html>
<head>
<title>Sistemes Informàtics - Mapa de la UF2</title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
<table cellpadding="10" style="border-style: solid; border-color: #AA6666;">
<tr style="background-color: #ffcccc; text-align: center; ">
<td colspan="5">UF2 Gestió de la informació i recursos de xarxa</td>
</tr>
<tr style="background-color: #fbd7cc; text-align: center;  ">
<td colspan="5">Fonaments de xarxes</td>
</tr>
<tr style="background-color: #f7f7cc; text-align: center;  ">
<td colspan="3">Introducció a les xarxes de computadors</td>
<td>Les xarxes d'àrea local</td>
<td>TCP/IP</td>
</tr>
<tr style="background-color: #f7f7cc; ">
<td>Representació  de la informació</td>
<td>Configuració xarxa a GNU/linux</td>
<td>Fonaments de protocols</td>
<td></td>
<td></td>
</tr>
</table>
</body>
</html>
