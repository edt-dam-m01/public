## Visors de documents en format Markdown

En aquest curs, treballarem sovint amb fitxers amb un format especial, els fitxers Markdown, amb una extesió .md

Per tal de poder visualitzar amb més comoditat aquests fitxers instal·leu al Firefox un complement (extensió) visor d'aquest tipus de documents.

En concret, us recomano el **Markdown Viewer** perquè també possibilita de veure documents markdown locals.

| ![Markdown Viewer](MarkdownViewer.png) |
|----------------------------------------|

Malauradament, el **Markdown Viewer** no està disponible per a les versions noves del Firefox (Quantum) i el procés és una mica més llarg:
- Si en la llista de les extensions disponibles no us apareix el Markdown Viewer o us diu que no és compatible amb la vostra versió del Firefox, instal·leu el **Markdown Viewer Webext** 
   | ![Markdown Viewer Webext](MarkdownViewerWebext.png) |
   |-----------------------------------------------------|
- A partir d'aquest moment, de vegades, quan obriu un fitxer markdown, us el mostrarà com a fitxer de text pla; llavors, feu clic sobre la icona ![MVW-icona](MVW-icona.png) a la barra d'eines del Firefox, i ja el podreu veure correctament.
- Quant als fitxers markdown locals, seguiu aquests passos:
   - `mkdir -p ~/.local/share/mime/packages`
   - Creeu el fitxer `~/.local/share/mime/packages/text-markdown.xml` amb el següent contingut:

      ```
      <?xml version="1.0"?>
      <mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
        <mime-type type="text/plain">
          <glob pattern="*.md"/>
          <glob pattern="*.mkd"/>
          <glob pattern="*.markdown"/>
        </mime-type>
      </mime-info>
      ```

   - `update-mime-database ~/.local/share/mime`
   - Ara, amb el Firefox, ja podeu obrir un fitxer Markdown local; la primera vegada us el mostrarà com a fitxer de text; llavors feu clic en la icona ![MVW-icona](MVW-icona.png) del **Markdown Viewer Webext**.

---

Si preferiu utilitzar el Chromium o el Google Chrome, podeu instal·lar l'extensió **Markdown Preview Plus**.
| ![Markdown Preview Plus](MarkdownPreviewPlus.png) |
|---------------------------------------------------|

