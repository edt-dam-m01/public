## Visors de documents en format Markdown

En aquest curs, treballarem sovint amb fitxers amb un format especial, els fitxers Markdown, amb una extesió .md

Per tal de poder visualitzar amb més comoditat aquests fitxers instal·leu al **Firefox** un complement (extensió) visor d'aquest tipus de documents.

Per a les versions noves del Firefox (Quantum), instal·leu el **Markdown Viewer Webext** 
   | ![Markdown Viewer Webext](MarkdownViewerWebext.png) |
   |-----------------------------------------------------|
- Malauradament, en aquest cas no podrem veure els fitxers markdown locals. Si voleu veure'ls, el procés és una mica més complicat. Seguiu aquests passos:
   - `mkdir -p ~/.local/share/mime/packages/`
   - Creeu el fitxer `~/.local/share/mime/packages/text-markdown.xml` amb el següent contingut:

      ```
      <?xml version="1.0"?>
      <mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
        <mime-type type="text/plain">
          <glob pattern="*.md"/>
          <glob pattern="*.mkd"/>
          <glob pattern="*.markdown"/>
        </mime-type>
      </mime-info>
      ```

   - `update-mime-database ~/.local/share/mime`
   - Ara, amb el Firefox, ja podeu obrir un fitxer Markdown local
   - Si teniu problemes amb la codificació dels caràctes feu "View → Character encoding → Unicode"

---

Si preferiu utilitzar el Chromium o el Google Chrome, podeu instal·lar l'extensió **Markdown Preview Plus**.
| ![Markdown Preview Plus](MarkdownPreviewPlus.png) |
|---------------------------------------------------|

- Per poder llegir fitxers locals, un cop s'instal·li heu d'activar l'opció: `Allow access to file URLs`
